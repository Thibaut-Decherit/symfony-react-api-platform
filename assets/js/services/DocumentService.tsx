import {isSsr} from '@/helpers/SsrHelper';

const setTitle = (title = '', baseTitle = 'SF React API Platform') => {
  if (isSsr()) {
    // document is undefined server side.
    return;
  }

  let fullTitle = baseTitle;

  if (title !== '') {
    fullTitle = title + ' | ' + baseTitle;
  }

  document.title = fullTitle;
}

export {
  setTitle
};
