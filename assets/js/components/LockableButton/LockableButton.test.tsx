import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {LockableButton} from './LockableButton';

describe('LockableButton', () => {
  test('basic rendering', () => {
    render(<LockableButton disabled={false}>test</LockableButton>);
    expect(screen.getByRole('button')).toBeVisible();
  });
});
