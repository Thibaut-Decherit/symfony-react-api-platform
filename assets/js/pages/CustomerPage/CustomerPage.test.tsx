import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import * as routes from '@/testing/routes.json';
import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {CustomerPage} from './CustomerPage';

describe.only('CustomerPage', () => {
  test('basic rendering', async () => {
    render(
      <Routes>
        <Route
          // Path doubles as new page, where id parameter will be 'new'.
          path={routes.react_app_customer_edit_page}
          element={<CustomerPage/>}
        />
      </Routes>,
      {route: routes.react_app_customer_new_page}
    );
    expect(await screen.findByRole('heading', {level: 1})).toBeVisible();
  });
});
