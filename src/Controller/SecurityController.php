<?php

namespace App\Controller;

use App\Entity\UserJWT;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractDefaultController
{
    /**
     * @param Request $request
     * @param JWTEncoderInterface $JWTEncoder
     * @Route("/logout", name="logout", methods={"POST"})
     * @return JsonResponse
     * @throws JWTDecodeFailureException
     */
    public function logout(Request $request, JWTEncoderInterface $JWTEncoder): JsonResponse
    {
        $JWTCookieName = $this->getParameter('app.auth_cookies_prefix') . $this->getParameter('app.jwt_cookie_name');

        $currentJWTCookie = $request->cookies->get($JWTCookieName);

        if (!is_null($currentJWTCookie)) {
            $em = $this->getDoctrine()->getManager();
            $userJWT = $em->getRepository(UserJWT::class)->findOneBy([
                'uniqueId' => $JWTEncoder->decode($currentJWTCookie)['uniqueId']
            ]);

            if (!is_null($userJWT)) {
                $em->remove($userJWT);
                $em->flush();
            }
        }

        $invalidatedJWTCookie = new Cookie(
            $JWTCookieName,
            null,
            -1,
            '/',
            null,
            $this->getParameter('app.auth_cookies_secure'),
            true,
            false,
            $this->getParameter('app.auth_cookies_same_site')
        );
        $response = new JsonResponse();
        $response->headers->setCookie($invalidatedJWTCookie);

        return $response;
    }
}
