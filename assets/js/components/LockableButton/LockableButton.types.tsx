import {ReactNode} from 'react';

export type ReactNodeType = ReactNode & {
  props?: {
    className?: string;
    [key: string]: unknown;
  }
};
