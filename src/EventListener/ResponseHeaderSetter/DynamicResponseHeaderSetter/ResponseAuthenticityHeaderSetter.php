<?php

namespace App\EventListener\ResponseHeaderSetter\DynamicResponseHeaderSetter;

use App\Service\SessionTokenService;
use Exception;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ResponseAuthenticityHeaderSetter
{
    private ResponseHeaderBag $responseHeaders;
    private SessionTokenService $sessionTokenService;

    public function __construct(
        ResponseHeaderBag $responseHeaders,
        SessionTokenService $sessionTokenService
    )
    {
        $this->responseHeaders = $responseHeaders;
        $this->sessionTokenService = $sessionTokenService;
    }

    /**
     * @throws Exception
     */
    public function set(): void
    {
        $this->responseHeaders->set(
            'app-response-authenticity',
            $this->sessionTokenService->get('response_authenticity')
        );
    }
}
