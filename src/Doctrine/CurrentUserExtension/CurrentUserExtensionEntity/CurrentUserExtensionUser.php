<?php

namespace App\Doctrine\CurrentUserExtension\CurrentUserExtensionEntity;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class CurrentUserExtensionUser implements CurrentUserExtensionEntityInterface
{
    public function addWhereCurrentUser(
        QueryBuilder $queryBuilder,
        string $rootAlias,
        string $userParameterName,
        QueryNameGeneratorInterface $queryNameGenerator
    ): void
    {
        $queryBuilder->andWhere("$rootAlias = :$userParameterName");
    }
}
