<?php

namespace App\Helper;

use Exception;
use InvalidArgumentException;

/**
 * Class RandomDataGeneratorHelper
 * Utility class for cryptographically secure pseudo-random data generation.
 *
 * @package App\Helper
 */
class RandomDataGeneratorHelper
{
    /**
     * Generates a pseudo-random float.
     * Warning: May or may not be cryptographically secure. Uses a cryptographically secure native function (random_int)
     * but this custom implementation could weaken randomness.
     *
     * @param float $min
     * @param float $max
     * @param int $maxDecimalNbr
     * @return float
     * @throws Exception
     */
    public static function randomFloat(float $min = 0, float $max = 2147483647, int $maxDecimalNbr = 1): float
    {
        if ($maxDecimalNbr < 1) {
            throw new InvalidArgumentException("\$maxDecimalNbr cannot be lower than 1, $maxDecimalNbr given");
        }

        $multiplier = 10 ** $maxDecimalNbr;

        return random_int($min * $multiplier, $max * $multiplier) / $multiplier;
    }

    /**
     * Generates a cryptographically secure pseudo-random integer.
     *
     * @param int $min
     * @param int $max
     * @return int
     * @throws Exception
     */
    public static function randomInteger(int $min = 0, int $max = 2147483647): int
    {
        return random_int($min, $max);
    }

    /**
     * Generates a URI safe base64 encoded cryptographically secure pseudo-random string that does not contain
     * "+", "/" or "=" which need to be URL encoded and make URLs unnecessarily longer.
     * With 512 bits of entropy this method will return a string of 86 characters, with 256 bits of entropy it will
     * return 43 characters, and so on.
     * String length is ceil($entropy / 6).
     *
     * @param int $entropy
     * @return string
     * @throws Exception
     */
    public static function randomString(int $entropy = 512): string
    {
        $bytes = random_bytes($entropy / 8);

        return rtrim(strtr(base64_encode($bytes), '+/', '-_'), '=');
    }
}
