<?php

namespace App\Command;

use App\Helper\StringHelper;
use App\Service\ReactAppRoutesGeneratorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class DumpReactAppRoutesToJsonCommand extends Command
{
    private string $kernelProjectDir;
    private string $outputFilePath;
    private ReactAppRoutesGeneratorService $routesGeneratorService;
    private Filesystem $filesystem;

    public function __construct(
        string $kernelProjectDir,
        string $outputFilePath,
        ReactAppRoutesGeneratorService $routesGeneratorService,
        Filesystem $filesystem
    )
    {
        parent::__construct();

        $this->kernelProjectDir = $kernelProjectDir;
        $this->outputFilePath = $outputFilePath;
        $this->routesGeneratorService = $routesGeneratorService;
        $this->filesystem = $filesystem;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:generate-react-app-routes-json')
            ->setDescription('Generates a JSON file of routes for consumption by the React app tests');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Generating routes JSON...');

        $this->filesystem->dumpFile($this->outputFilePath, json_encode($this->routesGeneratorService->generate()));

        $this->endOutput($output);
    }

    private function endOutput(OutputInterface $output): void
    {
        $relativeFilePath = StringHelper::removePrefix($this->outputFilePath, $this->kernelProjectDir . '/');
        $output->writeln("<bg=green;fg=black>[OK] React app routes dumped to $relativeFilePath</>");
    }
}
