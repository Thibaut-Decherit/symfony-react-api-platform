import {GetJWTResponse} from '@/services/APIService/AuthenticationAPIService.interfaces';
import type {GetJWTOnSuccessCallback} from '@/services/APIService/AuthenticationAPIService.types';
import type {Credentials} from '@/types';
import axios from 'axios';
import _ from 'lodash';

function getJWT(
  credentials: Credentials,
  onSuccess?: GetJWTOnSuccessCallback
) {
  return new Promise<void>((resolve, reject) => {
    axios
      .post('/api/login', credentials)
      .then((response: GetJWTResponse) => {
        if (_.has(response, 'data.token') && _.has(response, 'headers.app-user-front-end-data-json')) {
          if (typeof onSuccess === 'function') {
            onSuccess(response);
          }

          return resolve();
        }

        reject('error');
      })
      .catch(error => {
        if (_.has(error, 'response.status') && error.response.status === 401) {
          return reject('bad credentials');
        }

        if (_.has(error, 'message') && error.message === 'Network Error') {
          return reject('network error');
        }

        reject('error');
      });
  });
}

export {
  getJWT
};
