import type {ReactBootstrapFormControlChangeEventHandler} from '@/types';
import PropTypes from 'prop-types';
import React, {HTMLInputTypeAttribute} from 'react';
import {Badge} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';

const FormGroup = (
  {
    ariaLabel = undefined,
    errorMessages = [],
    label = undefined,
    name,
    onChange,
    placeholder = undefined,
    prefix,
    required = true,
    type = 'text',
    value = undefined
  }: {
    ariaLabel?: string;
    errorMessages?: string[];
    label?: string;
    name: string;
    onChange: ReactBootstrapFormControlChangeEventHandler;
    placeholder?: string;
    prefix: string;
    required?: boolean;
    type?: HTMLInputTypeAttribute;
    value?: string | number | string[];
  }
) => {
  const controlId = prefix + '-' + name + '-field';

  if (ariaLabel === undefined && label === undefined) {
    console.warn(
      `Accessibility: There are no label nor aria-label for field named '${name}' with id '${controlId}'`
    );
  }

  return (
    <Form.Group controlId={controlId}>
      {label !== undefined && (
        <Form.Label>{label}</Form.Label>
      )}
      {errorMessages.length > 0 && (
        <ul className="list-unstyled">
          {errorMessages.map((errorMessage, index) => {
            return (
              <li key={index} className="form-violation">
                <Badge variant="danger" className="mr-2">Error</Badge>
                {errorMessage}
              </li>
            );
          })}
        </ul>
      )}
      <Form.Control
        required={required} type={type} name={name} placeholder={placeholder} value={value} onChange={onChange}
        aria-label={ariaLabel}
      />
    </Form.Group>
  );
};

FormGroup.propTypes = {
  ariaLabel: PropTypes.string,
  errorMessages: PropTypes.arrayOf(PropTypes.string),
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  prefix: PropTypes.string.isRequired,
  required: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.number,
    PropTypes.string
  ])
};

export {
  FormGroup
};
