import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {InvoiceItem} from './InvoiceItem';

describe('InvoiceItem', () => {
  test('basic rendering', () => {
    render(
      <table>
        <tbody>
          <InvoiceItem
            handleDeleteCallback={jest.fn()}
            invoice={{
              amount: '1',
              chrono: 1,
              customer: {
                company: null,
                email: 'test@test.test',
                firstName: 'test',
                id: 1,
                lastName: 'test',
              },
              id: 1,
              sentAt: '2022-03-02T11:41:12+01:00',
              status: 'SENT'
            }}
          />
        </tbody>
      </table>
    );
    expect(screen.getByRole('row')).toBeVisible();
  });
});
