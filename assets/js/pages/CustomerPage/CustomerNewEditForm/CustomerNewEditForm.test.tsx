import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {CustomerNewEditForm} from './CustomerNewEditForm';

describe('CustomerNewEditForm', () => {
  test('basic rendering', () => {
    render(<CustomerNewEditForm
      initialStateCustomer={{
        company: '',
        email: '',
        firstName: '',
        lastName: ''
      }}
      parentStateCustomer={{
        company: '',
        email: '',
        firstName: '',
        lastName: ''
      }}
      setParentStateCustomer={jest.fn()}
      id={'new'}
    />);
    expect(screen.getByRole('form')).toBeVisible();
  });
});
