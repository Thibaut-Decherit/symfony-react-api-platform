import PropTypes from 'prop-types';
import React, {useCallback} from 'react';
import Pagination from 'react-bootstrap/Pagination';

const PaginatorItem = (
  {
    currentPageNumber,
    onClick,
    pageNumber
  }: {
    currentPageNumber: number;
    onClick: (pageNumber: number) => void;
    pageNumber: number;
  }
) => {
  const handleClick = useCallback(() => {
    onClick(pageNumber);
  }, [onClick, pageNumber]);

  return (
    <Pagination.Item
      active={pageNumber === currentPageNumber}
      key={pageNumber}
      onClick={handleClick}
    >
      {pageNumber}
    </Pagination.Item>
  );
};

PaginatorItem.propTypes = {
  currentPageNumber: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  pageNumber: PropTypes.number.isRequired
};

export {
  PaginatorItem
};
