<?php

namespace App\Repository;

use App\Entity\UserJWT;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserJWT|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserJWT|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserJWT[]    findAll()
 * @method UserJWT[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserJWTRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserJWT::class);
    }

    /**
     * @return int The number of removed entries.
     */
    public function removeExpired(): int
    {
        $qb = $this->createQueryBuilder('uj');
        $qb
            ->delete()
            ->where('uj.expirationAt <= CURRENT_TIMESTAMP()');

        return $qb->getQuery()->getResult();
    }
}
