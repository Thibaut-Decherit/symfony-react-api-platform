import {useFocus} from '@/components/hooks/useFocus';
import {useMaintainFocus} from '@/components/hooks/useMaintainFocus';
import {LockableButton} from '@/components/LockableButton';
import type {ReactBootstrapFormControlChangeEvent, ReactBootstrapFormSubmitEventHandler} from '@/types';
import PropTypes from 'prop-types';
import React, {useCallback, useEffect, useState} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import type {ButtonVariant} from 'react-bootstrap/types';

const SearchBar = (
  {
    ariaLabel = undefined,
    buttonLabel = 'Search',
    buttonPosition = 'append',
    buttonVariant = 'primary',
    defaultValue = '',
    disabled = false,
    onSubmit,
    placeholder = undefined
  }: {
    ariaLabel?: string;
    buttonLabel?: string;
    buttonPosition?: 'append' | 'prepend';
    buttonVariant?: ButtonVariant;
    defaultValue?: string;
    disabled?: boolean;
    onSubmit: (searchValue: string) => void;
    placeholder?: string;
  }
) => {
  const [stateSearchInput, setStateSearchInput] = useState(defaultValue);
  const [stateTypingTimer, setStateTypingTimer] = useState<NodeJS.Timeout | null>(null);
  const [maintainFocus, toggleMaintainFocusOnCallback, toggleMaintainFocusOffCallback] = useMaintainFocus();

  const handleSearchInput = useCallback((event: ReactBootstrapFormControlChangeEvent) => {
    const input = event.currentTarget.value;

    setStateSearchInput(input);

    setStateTypingTimer(setTimeout(() => {
      onSubmit(input);
    }, 500));
  }, [onSubmit]);

  const handleSubmit = useCallback((event: ReactBootstrapFormSubmitEventHandler) => {
    event.preventDefault();

    // We don't want to needlessly submit the search again when the timer runs out.
    if (stateTypingTimer !== null) {
      clearTimeout(stateTypingTimer);
    }

    onSubmit(stateSearchInput);
  }, [onSubmit, stateSearchInput, stateTypingTimer]);

  useEffect(() => {
    /*
    Called every time stateTypingTimer changes, then runs clearTimeout() on the previous value of stateTypingTimer.
    Also called when the component is unmounted.
     */
    return () => {
      if (stateTypingTimer !== null) {
        clearTimeout(stateTypingTimer);
      }
    };
  }, [stateTypingTimer]);

  // Ensures stateSearchInput remains synced with defaultValue prop in case the parent changes it.
  useEffect(() => {
    setStateSearchInput(defaultValue);
  }, [defaultValue]);

  const [focusRef, setFocus] = useFocus<HTMLInputElement>('endOnFirstCall');
  useEffect(() => {
    if (!disabled && maintainFocus) {
      // If the user was focusing the field before it got disabled, sets focus back once the field is re-enabled.
      setFocus();
    }
  }, [disabled, setFocus, maintainFocus]);

  return (
    <Form onSubmit={handleSubmit}>
      <InputGroup className="mb-3">
        {buttonPosition === 'prepend' && (
          <InputGroup.Prepend>
            <Button type="submit" variant={buttonVariant}>{buttonLabel}</Button>
          </InputGroup.Prepend>
        )}
        <Form.Control
          aria-label={ariaLabel !== null ? ariaLabel : placeholder}
          disabled={disabled}
          onBlur={toggleMaintainFocusOffCallback}
          onChange={handleSearchInput}
          onFocus={toggleMaintainFocusOnCallback}
          placeholder={placeholder}
          ref={focusRef}
          value={stateSearchInput}
        />
        {buttonPosition === 'append' && (
          <InputGroup.Append>
            <LockableButton disabled={disabled} type="submit" variant={buttonVariant}>
              <span aria-hidden={true} className="fa fa-magnifying-glass mr-2" data-lockable-button-icon=""/>
              {buttonLabel}
            </LockableButton>
          </InputGroup.Append>
        )}
      </InputGroup>
    </Form>
  );
};

SearchBar.propTypes = {
  ariaLabel: PropTypes.string,
  buttonLabel: PropTypes.string,
  buttonPosition: PropTypes.string,
  buttonVariant: PropTypes.oneOf([
    'append',
    'prepend'
  ]),
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  placeholder: PropTypes.string
};

export {
  SearchBar
};
