import {TwigDataService} from '@/services/TwigDataService';
import PropTypes from 'prop-types';
import React, {useCallback, useState} from 'react';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import type {ButtonVariant} from 'react-bootstrap/types';

const DeleteButton = (
  {
    block = false,
    disabled = false,
    handleDelete,
    label = 'Delete',
    size = 'sm',
    variant = 'danger'
  }: {
    disabled?: boolean;
    block?: boolean;
    handleDelete: () => Promise<void>;
    label?: string;
    size?: 'lg' | 'sm';
    variant?: ButtonVariant;
  }
) => {
  const [stateConfirmationButtonsInteractive, setStateConfirmationButtonsInteractive] = useState(false);
  const [statePendingConfirmation, setStatePendingConfirmation] = useState(false);
  const [statePendingDelete, setStatePendingDelete] = useState(false);

  const showConfirmationButtons = useCallback(() => {
    setStatePendingConfirmation(true);

    // Prevents accidental delete if the button is double-clicked.
    setTimeout(() => {
      setStateConfirmationButtonsInteractive(true);
    }, 250);
  }, []);

  const handleDeleteButtonClick = useCallback(() => {
    if (!stateConfirmationButtonsInteractive) {
      // Prevents accidental delete if the initial button is double clicked.
      return;
    } else {
      setStateConfirmationButtonsInteractive(false);
    }

    setStatePendingDelete(true);
    setStatePendingConfirmation(false);

    // Button goes back to initial state if delete request fails.
    handleDelete()
      .catch(() => {
        setStatePendingDelete(false);
      });
  }, [stateConfirmationButtonsInteractive, handleDelete]);

  const handleCancelButtonClick = useCallback(() => {
    // Prevents accidental toggle if the initial button is double clicked.
    if (!stateConfirmationButtonsInteractive) {
      return;
    } else {
      setStateConfirmationButtonsInteractive(false);
    }

    setStatePendingConfirmation(false);
  }, [stateConfirmationButtonsInteractive]);

  const renderDeleteButton = useCallback(() => {
    const getLabel = () => {
      if (statePendingDelete) {
        return (
          <Spinner animation="border" role="status" size="sm">
            <span className="sr-only">{TwigDataService.get('translations.global.loading')}</span>
          </Spinner>
        );
      }

      return label;
    };

    return (
      <Button
        onClick={showConfirmationButtons} variant={variant} size={size}
        disabled={statePendingDelete || disabled} block={block}
      >
        {getLabel()}
      </Button>
    );
  }, [block, disabled, label, statePendingDelete, showConfirmationButtons, size, variant]);

  const renderConfirmationButtons = useCallback(() => {
    return (
      <div className="delete-button-confirmation-buttons-group">
        <Button
          onClick={handleDeleteButtonClick} variant="success" className="w-50"
          aria-label="Confirm deletion" size={size}
        >
          Y
        </Button>
        <Button
          onClick={handleCancelButtonClick} variant="danger" className="w-50"
          aria-label="Cancel deletion" size={size}
        >
          N
        </Button>
      </div>
    );
  }, [handleCancelButtonClick, handleDeleteButtonClick, size]);

  if (statePendingConfirmation) {
    return renderConfirmationButtons();
  }

  return renderDeleteButton();
};

DeleteButton.propTypes = {
  block: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  handleDelete: PropTypes.func.isRequired,
  size: PropTypes.string,
  variant: PropTypes.string
};

export {
  DeleteButton
};
