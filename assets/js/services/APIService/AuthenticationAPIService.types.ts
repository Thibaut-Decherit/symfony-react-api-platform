import {GetJWTResponse} from '@/services/APIService/AuthenticationAPIService.interfaces';

export type GetJWTOnSuccessCallback = ((response: GetJWTResponse) => void);
