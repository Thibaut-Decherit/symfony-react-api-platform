import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {LogoutButton} from './LogoutButton';

describe('LogoutButton', () => {
  test('basic rendering', () => {
    render(<LogoutButton/>);
    expect(screen.getByRole('button')).toBeVisible();
  });
});
