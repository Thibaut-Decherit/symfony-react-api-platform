import {GlobalContext} from '@/GlobalContextProvider';
import {ObjectHelper} from '@/helpers/ObjectHelper';
import {CustomerNewEditForm} from '@/pages/CustomerPage/CustomerNewEditForm';
import type {CustomerPageCustomer} from '@/pages/CustomerPage/CustomerPage.types';
import {findOneById} from '@/services/APIService/CustomerAPIService';
import {setTitle} from '@/services/DocumentService';
import React, {useContext, useEffect, useState} from 'react';
import {Col, Row} from 'react-bootstrap';
import {useParams} from 'react-router-dom';

/*
Declared outside the component to preserve the value between the component's instances. Must be set to true in a
useEffect() after first client side render.
 */
let ignoreSsrProps = false;

const CustomerPage = () => {
  const globalContext = useContext(GlobalContext);
  const ssrProps: { customer?: CustomerPageCustomer } | undefined = globalContext?.routeSsrProps?.CustomerPage;

  const initialStateCustomer: CustomerPageCustomer = {
    company: '',
    email: '',
    firstName: '',
    lastName: ''
  };
  const [stateCustomer, setStateCustomer] = useState<CustomerPageCustomer>(
    !ignoreSsrProps
      ? ObjectHelper.updateSharedKeys<CustomerPageCustomer>(initialStateCustomer, ssrProps?.customer || {})
      : {...initialStateCustomer}
  );
  const {id} = useParams();

  if (typeof id !== 'string' || (id.match(/\d+/) === null && id !== 'new')) {
    /*
    Only used to enforce Typescript id type. Should never be thrown as if id is invalid it means the user manually typed
    an invalid value in the URL and has been redirected to a 404 page (SSR page route only support 'new' or a number
    cast to string for the id parameter).
     */
    throw Error('id type or content is not supported.');
  }

  // If false, it's a new form instead of an edit form, because edit value is 'new' instead of an integer.
  const isEdit = id.match(/\d+/) !== null;

  /*
  - If !isEdit, then id is not an integer and its value is 'new', so the form is in new mode and no need to show the
  loading message because we don't have a customer to fetch.
  - If isEdit and !ignoreSsrProps we may have to fill the form with data from ssrProps?.customer (coming
  from the server in props passed during SSR), so no need to show the loading message until data is fetched: it's
  already available in ssrProps?.customer.
  - If isEdit and ignoreSsrProps, then it means the component has been accessed at least once already, so
  we ignore ssrProps?.customer and we need to show the loading message because we have a customer to fetch.
  - If isEdit, !ignoreSsrProps and ssrProps?.customer === undefined, then it means the component has been
  accessed via react-router-dom without being rendered server side and is in edit mode, so we have a customer to
  fetch.
   */
  const [stateIsLoading, setStateIsLoading] = useState(
    isEdit && (ignoreSsrProps || ssrProps?.customer === undefined)
  );

  useEffect(() => {
    // We already have a customer passed via SSR, or it's not an edit form, so no need to fetch a customer.
    if ((!ignoreSsrProps && ssrProps?.customer !== undefined) || id.match(/\d+/) === null) {
      ignoreSsrProps = true;

      return;
    }

    setStateIsLoading(true);

    findOneById(id)
      .then(customer => {
        setStateCustomer({
          company: customer.company,
          email: customer.email,
          firstName: customer.firstName,
          lastName: customer.lastName
        });

        setStateIsLoading(false);
      });
  }, [id, ssrProps?.customer]);

  setTitle(isEdit ? 'Edit customer' : 'Add customer');

  return (
    <>
      <Row className="justify-content-center">
        <Col md="6">
          {stateIsLoading ? (
            <h1>Loading...</h1>
          ) : (
            <>
              <h1>
                {id.match(/\d+/) !== null
                  ? `Edit customer ${stateCustomer.firstName} ${stateCustomer.lastName}`
                  : 'Add a new customer'
                }
              </h1>
              <CustomerNewEditForm
                initialStateCustomer={initialStateCustomer} parentStateCustomer={stateCustomer}
                setParentStateCustomer={setStateCustomer} id={id}
              />
            </>
          )}
        </Col>
      </Row>
    </>
  );
};

export {
  CustomerPage
};
