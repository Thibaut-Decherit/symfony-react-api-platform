import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {LoginPage} from './LoginPage';

describe('LoginPage', () => {
  test('basic rendering', () => {
    render(<LoginPage/>);
    expect(screen.getByRole('heading', {level: 1})).toBeVisible();
  });
});
