import {DeleteButton} from '@/components/DeleteButton';
import type {InvoiceListInvoice} from '@/pages/InvoiceIndexPage/InvoicesList/InvoicesList.types';
import PropTypes from 'prop-types';
import React, {useCallback, useMemo} from 'react';
import Button from 'react-bootstrap/Button';

const InvoiceItem = (
  {
    invoice,
    handleDeleteCallback
  }: {
    invoice: InvoiceListInvoice;
    handleDeleteCallback: (invoiceId: number) => Promise<void>;
  }
) => {
  const invoiceBadgeVariants: {
    [key: string]: string
  } = {
    CANCELLED: 'badge-danger',
    PAID: 'badge-success',
    SENT: 'badge-warning'
  };

  /*
  async/await seems to be needed, else deleting an item throws the "handleDelete() is not a function" error when it is
  time to call .then(), .catch() or .finally() on it (see inside assets/js/components/DeleteButton.jsx
  handleDeleteButtonClick() function). This does not occur without useCallback() or if the promise returned by
  handleDeleteButtonClick() is ignored.
   */
  const handleDelete = useCallback(async () => {
    await handleDeleteCallback(invoice.id);
  }, [invoice.id, handleDeleteCallback]);

  const dateTimeFormatter = useMemo(() => {
    return new Intl.DateTimeFormat();
  }, []);

  return (
    <tr>
      <td>{invoice.chrono}</td>
      <td>
        <a href="#">{invoice.customer.firstName} {invoice.customer.lastName} [{invoice.customer.id}]</a>
      </td>
      <td>
        <a href={`mailto:${invoice.customer.email}`}>{invoice.customer.email}</a>
      </td>
      <td>{invoice.customer.company || 'n/a'}</td>
      <td className="text-center">{parseFloat(invoice.amount).toLocaleString()} €</td>
      <td className="text-center">{dateTimeFormatter.format(new Date(invoice.sentAt))}</td>
      <td className="text-center">
        <span className={`badge ${invoiceBadgeVariants[invoice.status]}`}>
          {invoice.status}
        </span>
      </td>
      <td>
        <Button aria-label="Edit" variant="primary">
          Edit
        </Button>
        <DeleteButton block disabled={invoice.status !== 'CANCELLED'} handleDelete={handleDelete}/>
      </td>
    </tr>
  );
};

InvoiceItem.propTypes = {
  invoice: PropTypes.shape({
    amount: PropTypes.string.isRequired,
    chrono: PropTypes.number.isRequired,
    customer: PropTypes.shape({
      company: PropTypes.string,
      email: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      lastName: PropTypes.string.isRequired,
    }).isRequired,
    id: PropTypes.number.isRequired,
    sentAt: PropTypes.string.isRequired,
    status: PropTypes.oneOf([
      'CANCELLED',
      'PAID',
      'SENT'
    ]),
  }).isRequired,
  handleDeleteCallback: PropTypes.func.isRequired
};

export {
  InvoiceItem
};
