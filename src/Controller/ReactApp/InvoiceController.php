<?php

namespace App\Controller\ReactApp;

use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route(path="/invoice")
 */
class InvoiceController extends AbstractReactAppController
{
    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param InvoiceRepository $invoiceRepository
     * @Route(path="/index", name="react_app_invoice_index_page")
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function index(
        Request $request,
        SerializerInterface $serializer,
        InvoiceRepository $invoiceRepository
    ): Response
    {
        $searchParams = $this->cleanupSearchParams(Invoice::class, $request->query->all());

        // Redirects to current route but with cleaned up search parameters.
        if ($searchParams['has_errors']) {
            return $this->redirectToRoute('react_app_invoice_index_page', $searchParams['search_params']);
        }

        $invoices = $invoiceRepository->reactAppIndexSearch($searchParams['search_params'], $this->getUser());

        return $this->render('app.html.twig', [
            'props' => $this->buildProps(
                $request,
                [
                    'InvoiceIndexPage' => [
                        'InvoicesList' => [
                            /*
                             * serialize() applies serialization groups to limit exposed data and converts entities to
                             * JSON. Then we call json_decode() because react_component() Twig function expects objects
                             * and handles the JSON encoding itself.
                             */
                            'invoices' => json_decode($serializer->serialize($invoices, 'jsonld')),
                            'searchParams' => $searchParams['search_params']
                        ]
                    ]
                ]
            ),
            'title' => $this->buildDocumentTitle('Invoices')
        ]);
    }
}
