import {FormGroup} from '@/components/FormGroup';
import {LockableButton} from '@/components/LockableButton';
import {GlobalContext} from '@/GlobalContextProvider';
import {LodashHelper} from '@/helpers/LodashHelper';
import {URLHelper} from '@/helpers/URLHelper';
import {login} from '@/services/AuthenticationService';
import type {GlobalFormAlert, ReactBootstrapFormControlChangeEvent, ReactBootstrapFormSubmitEventHandler} from '@/types';
import _ from 'lodash';
import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import Alert from 'react-bootstrap/Alert';
import Form from 'react-bootstrap/Form';
import {useLocation} from 'react-router-dom';

/*
Declared outside the component to preserve the value between the component's instances. Must be set to true in a
useEffect() after first client side render.
 */
let ignoreSsrProps = false;

const LoginForm = () => {
  const globalContext = useContext(GlobalContext);
  const ssrProps = globalContext.routeSsrProps?.LoginPage?.LoginForm;
  const location = useLocation();

  const initialStateCredentials = {
    username: '',
    password: ''
  };
  const [stateCredentials, setStateCredentials] = useState({...initialStateCredentials});

  /*
  useMemo() to preserve the values between renders, else after first render the value will change in case of SSR
  because ignoreSsrProps will no longer be false.
   */
  const fromUrlRelative = useMemo(() => {
    if (_.has(location, 'state.from.pathname')) {
      return location.state.from.pathname;
    } else if (!ignoreSsrProps && _.has(ssrProps, 'fromUrlRelative')) {
      return ssrProps.fromUrlRelative;
    }

    return globalContext.ssrRoutes.react_app_home_page;
  }, [globalContext.ssrRoutes.react_app_home_page, location, ssrProps])
  const fromUrlAbsolute = useMemo<false | string>(() => {
    // Prevents display of the alert message if the user comes from the homepage as it's redundant.
    if (fromUrlRelative === globalContext.ssrRoutes.react_app_home_page) {
      return false;
    }

    return URLHelper.buildAbsolute(fromUrlRelative);
  }, [fromUrlRelative, globalContext.ssrRoutes.react_app_home_page]);

  const initialStateGlobalFormAlert = useMemo(() => {
    return {};
  }, []);
  const [stateGlobalFormAlert, setStateGlobalFormAlert] = useState<GlobalFormAlert>({...initialStateGlobalFormAlert});
  const [stateIsLoading, setStateIsLoading] = useState(false);

  const handleChange = useCallback(({currentTarget}: ReactBootstrapFormControlChangeEvent) => {
    const {name, value} = currentTarget;

    setStateCredentials({...stateCredentials, [name]: value});
  }, [stateCredentials]);

  const handleSubmit = useCallback(async (event: ReactBootstrapFormSubmitEventHandler) => {
    event.preventDefault();

    setStateGlobalFormAlert({...initialStateGlobalFormAlert});
    setStateIsLoading(true);

    let loginResponse;
    try {
      loginResponse = await login(
        stateCredentials,
        response => {
          globalContext.setStateUserInfo(JSON.parse(response.headers['app-user-front-end-data-json']));
        }
      );
    } catch (error) {
      loginResponse = error;
    }

    switch (loginResponse) {
      case 'success':
        // Success will trigger a redirect in LoginPage, so we can stop code execution here.
        return;
      case 'bad credentials':
        setStateGlobalFormAlert({variant: 'danger', message: 'Wrong email or password'});
        break;
      case 'network error':
        setStateGlobalFormAlert({variant: 'danger', message: 'Network Error'});
        break;
      default:
        setStateGlobalFormAlert({variant: 'danger', message: 'Something went wrong'});
    }

    setStateCredentials({...stateCredentials, password: ''});
    setStateIsLoading(false);
  }, [globalContext, initialStateGlobalFormAlert, stateCredentials]);

  useEffect(() => {
    if (!ignoreSsrProps) {
      ignoreSsrProps = true;
    }
  }, []);

  const formPrefix = 'login-form';
  return (
    <>
      {LodashHelper.hasAll(stateGlobalFormAlert, ['variant', 'message']) && (
        <Alert variant={stateGlobalFormAlert.variant} className="text-center">
          {stateGlobalFormAlert.message}
        </Alert>
      )}

      {!location.state?.skipFrom && fromUrlAbsolute !== false && (
        <Alert className={'text-center'} variant={'danger'}>
          Vous devez vous connecter pour accéder à la page&nbsp;
          <span className={'font-weight-bold'}>{fromUrlAbsolute}</span>.
        </Alert>
      )}

      <Form aria-label={'login form'} onSubmit={handleSubmit}>
        <FormGroup
          prefix={formPrefix} name="username" onChange={handleChange} type="email" label="Email address"
          value={stateCredentials.username}
        />
        <FormGroup
          prefix={formPrefix} name="password" onChange={handleChange} type="password" label="Password"
          value={stateCredentials.password}
        />

        <LockableButton disabled={stateIsLoading} variant="primary" type="submit">
          <span aria-hidden={true} className="fa fa-paper-plane mr-2" data-lockable-button-icon=""/>
          Submit
        </LockableButton>
      </Form>
    </>
  );
};

export {
  LoginForm
};
