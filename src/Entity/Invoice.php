<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\CustomerNameOrCompanyFilter;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups as SerializerGroups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=true,
 *          "pagination_items_per_page"=20,
 *          "order"={"sentAt"="desc"},
 *     },
 *     itemOperations={
 *          "delete"={
 *              "security"="is_granted('INVOICE_DELETE', object)"
 *          },
 *          "get",
 *          "patch",
 *          "put"
 *     },
 *     normalizationContext={
 *          "groups"={"invoice_read"}
 *     },
 *     subresourceOperations={
 *          "api_customers_invoices_read_subresource"={
 *              "normalization_context"={
 *                  "groups"={"invoice_read_as_subresource"}
 *              }
 *          }
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "customer.firstName": "start",
 *     "customer.lastName": "start",
 *     "customer.company": "start"
 * })
 * @ApiFilter(OrderFilter::class, properties={"amount", "sentAt"})
 * @ApiFilter(CustomerNameOrCompanyFilter::class, strategy="start")
 */
class Invoice
{
    const STATUS_CANCELLED = 'CANCELLED';
    const STATUS_PAID = 'PAID';
    const STATUS_SENT = 'SENT';

    /**
     * Do not remove, it is used in $this->status Assert\Choice() constraint.
     *
     * @var array
     */
    const STATUSES = [
        self::STATUS_CANCELLED,
        self::STATUS_PAID,
        self::STATUS_SENT
    ];

    /**
     * Do not remove, it is used dynamically in
     * App\Controller\ReactApp\AbstractReactAppController::cleanupSearchParams().
     *
     * @var array
     */
    const DEFAULT_SEARCH_PARAMS = [
        'itemsPerPage' => 5,
        'page' => 1
    ];

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @SerializerGroups({"invoice_read", "customer_read", "invoice_read_as_subresource"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", scale="2")
     * @SerializerGroups({"invoice_read", "customer_read", "invoice_read_as_subresource"})
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     * @Assert\PositiveOrZero()
     */
    private string $amount;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     * @SerializerGroups({"invoice_read", "customer_read", "invoice_read_as_subresource"})
     * @Assert\DateTime(message="form_errors.global.valid_date")
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $sentAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @SerializerGroups({"invoice_read", "customer_read", "invoice_read_as_subresource"})
     * @Assert\Choice(Invoice::STATUSES)
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $status;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false)
     * @SerializerGroups({"invoice_read", "invoice_read_as_subresource"})
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $customer;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     * @SerializerGroups({"invoice_read", "customer_read", "invoice_read_as_subresource"})
     */
    private $chrono;

    /**
     * @SerializerGroups({"invoice_read", "invoice_read_as_subresource"})
     * @return User
     */
    public function getUser(): User
    {
        return $this->getCustomer()->getUser();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getSentAt(): ?DateTime
    {
        return $this->sentAt;
    }

    /**
     * @param DateTime $sentAt
     * @return $this
     */
    public function setSentAt(DateTime $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @SerializerGroups({"invoice_read", "invoice_read_as_subresource"})
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getChrono(): ?int
    {
        return $this->chrono;
    }

    /**
     * @param int|null $chrono
     * @return $this
     */
    public function setChrono(?int $chrono): self
    {
        $this->chrono = $chrono;

        return $this;
    }
}
