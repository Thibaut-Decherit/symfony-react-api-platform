import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {InvoicesList} from './InvoicesList';

describe('InvoicesList', () => {
  test('basic rendering', () => {
    render(<InvoicesList/>);
    expect(screen.getByRole('table')).toBeVisible();
  });
});
