import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {CustomersList} from './CustomersList';

describe('CustomersList', () => {
  test('basic rendering', () => {
    render(<CustomersList/>);
    expect(screen.getByRole('table')).toBeVisible();
  });
});
