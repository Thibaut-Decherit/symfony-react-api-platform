import {DropdownButton} from '@/components/DropdownButton';
import {Paginator} from '@/components/Paginator';
import {SearchBar} from '@/components/SearchBar';
import {GlobalContext} from '@/GlobalContextProvider';
import {ReactRouterDomHelper} from '@/helpers/ReactRouterDomHelper';
import {CustomerItem} from '@/pages/CustomerIndexPage/CustomersList/CustomerItem';
import type {CustomerListCustomer} from '@/pages/CustomerIndexPage/CustomersList/CustomersList.types';
import {deleteOneById, paginatedFindByNameOrCompanyStartsBy} from '@/services/APIService/CustomerAPIService';
import _ from 'lodash';
import React, {useCallback, useContext, useEffect, useMemo, useReducer, useState} from 'react';
import {useSearchParams} from 'react-router-dom';

/*
Declared outside the component to preserve the value between the component's instances. Must be set to true in a
useEffect() after first client side render.
 */
let ignoreSsrProps = false;

const defaultStatesValues = {
  currentPageNumber: 1,
  customersPerPage: 5,
  searchValue: ''
};

const reducerStateCurrentPageNumber = (
  state: number,
  action: {
    data?: number | string | null;
    type: 'update';
  }
): number => {
  switch (action.type) {
    case 'update': {
      let newData = action?.data;
      if (typeof newData === 'string') {
        newData = Number(newData);
      } else if (newData === null || newData === undefined) {
        newData = defaultStatesValues.currentPageNumber
      }

      return newData;
    }

    default:
      throw Error('Unknown action.');
  }
};

const reducerStateCustomersPerPage = (
  state: number,
  action: {
    data: number | string | null;
    type: 'update';
  }
): number => {
  switch (action.type) {
    case 'update': {
      let newData = action?.data;
      if (typeof newData === 'string') {
        newData = Number(newData);
      } else if (newData === null || newData === undefined) {
        newData = defaultStatesValues.customersPerPage
      }

      return newData;
    }
  }
  throw Error('Unknown action.');
};

const reducerStateSearchValue = (
  state: string,
  action: {
    data?: number | string | null;
    type: 'update';
  }
): string => {
  switch (action.type) {
    case 'update': {
      let newData = action?.data;
      if (typeof newData === 'number') {
        newData = String(newData);
      } else if (newData === null || newData === undefined) {
        newData = defaultStatesValues.searchValue
      }

      return newData;
    }

    default:
      throw Error('Unknown action.');
  }
};

const CustomersList = () => {
  const globalContext = useContext(GlobalContext);
  const ssrProps = globalContext.routeSsrProps?.CustomerIndexPage?.CustomersList;
  const ssrCustomers: {
    results?: CustomerListCustomer[],
    totalItemsCount?: number
  } | undefined = ssrProps?.customers;
  const [searchParams, setSearchParams] = useSearchParams();

  const [stateCurrentPageNumber, dispatchStateCurrentPageNumber] = useReducer(
    reducerStateCurrentPageNumber,
    !ignoreSsrProps
      ? Number(_.get(ssrProps, 'searchParams.page', defaultStatesValues.currentPageNumber))
      // Always returns null during SSR, so we need to pass it via ssrProps, or we will always get the default value.
      : Number(_.defaultTo(searchParams.get('page'), defaultStatesValues.currentPageNumber))
  );
  const setStateCurrentPageNumber = (newState: number | string | null) => {
    dispatchStateCurrentPageNumber({data: newState, type: 'update'});
  };
  const [stateCustomers, setStateCustomers] = useState(
    !ignoreSsrProps
      /*
      It's the first time the component is instantiated. Maybe it got server side rendered, in which case
      ssrCustomers.results contains a list of customers generated server side, so we can display it.
      But maybe the component did not get server side rendered and the user just navigated to it via another component,
      in which case ssrCustomers?.results is undefined, so we initialize the list as an empty array and useEffect() will
      call getCustomerIndexPage() to populate it.
       */
      ? ssrCustomers?.results || []
      /*
      It's not the first time the component is instantiated, so we initialize the list as an empty array and useEffect()
      will call getCustomerIndexPage() to populate it with up-to-date data. We ignore ssrCustomers?.results even if it
      exists because it may not be up-to-date anymore.
       */
      : []
  );
  const [stateCustomersPerPage, dispatchStateCustomersPerPage] = useReducer(
    reducerStateCustomersPerPage,
    !ignoreSsrProps
      ? Number(_.get(ssrProps, 'searchParams.itemsPerPage', defaultStatesValues.customersPerPage))
      // Always returns null during SSR, so we need to pass it via ssrProps, or we will always get the default value.
      : Number(_.defaultTo(searchParams.get('itemsPerPage'), defaultStatesValues.customersPerPage))
  );
  const setStateCustomersPerPage = (newState: number | string | null) => {
    dispatchStateCustomersPerPage({data: newState, type: 'update'});
  };
  const [stateError, setStateError] = useState<boolean | string>(false);

  /*
  If !ignoreSsrProps we may have to populate the list with data from ssrCustomers (coming from the server in props
  passed during SSR), so no need to show the loading message until data is fetched: it's already available in
  ssrCustomers.
  Except if ssrCustomers is undefined, then it means the component has been accessed via react-router-dom, so we need to
  show the loading message because we have data to fetch.
  If ignoreSsrProps, it means the component has been accessed at least once already, so we ignore ssrCustomers (it could
  be outdated) and we need to show the loading message because we have up-to-date customers to fetch.
   */
  const [stateIsLoading, setStateIsLoading] = useState(
    !ignoreSsrProps
      ? ssrCustomers === undefined
      : true
  );

  const [stateSearchValue, dispatchStateSearchValue] = useReducer(
    reducerStateSearchValue,
    !ignoreSsrProps
      ? _.get(ssrProps, 'searchParams.search', defaultStatesValues.searchValue)
      // Always returns null during SSR, so we need to pass it via ssrProps, or we will always get the default value.
      : _.defaultTo(searchParams.get('search'), defaultStatesValues.searchValue)
  );
  const setStateSearchValue = (newState: number | string | null) => {
    dispatchStateSearchValue({data: newState, type: 'update'});
  };
  const [stateTotalCustomersCount, setStateTotalCustomersCount] = useState(
    !ignoreSsrProps
      ? ssrCustomers?.totalItemsCount || 0
      : 0
  );

  const getCustomerIndexPage = useCallback(() => {
    setStateError(false);
    setStateIsLoading(true);

    paginatedFindByNameOrCompanyStartsBy(
      stateCustomersPerPage,
      stateCurrentPageNumber,
      stateSearchValue
    )
      .then(
        response => {
          setStateCustomers(response.results);
          setStateTotalCustomersCount(response.totalItemsCount);
        })
      .catch((error: Error) => {
        setStateCustomers([]);
        setStateTotalCustomersCount(0);

        if (error.message === 'Network Error') {
          setStateError('Network error');
        } else {
          setStateError('Something went wrong');
        }
      })
      .finally(() => {
        setStateIsLoading(false);
      })
  }, [stateCurrentPageNumber, stateCustomersPerPage, stateSearchValue]);

  /*
   useEffect hook is called on render.
   If optional second parameter is:
      - not specified, the hook is called on each render
      - an empty array, the hook is called only on the first render of the component
      - an array containing one or multiple variables (most probably states), the hook is called on render if one of
      the values has changed since previous render
   See https://reactjs.org/docs/hooks-effect.html
   */
  useEffect(() => {
    /*
    It's the first time the component is instantiated. Maybe it got server side rendered, maybe the user
    just navigated to it via another component.
    */
    if (!ignoreSsrProps) {
      ignoreSsrProps = true;

      /*
      If we just server side rendered the component (ssrCustomers is defined) the server already passed a
      list of customers via props, so we don't need to fetch a new one until the user interacts with the
      paginator, the items per page dropdown or the search bar.
       */
      if (ssrCustomers !== undefined) {
        return;
      }
    }

    /*
    If we didn't just server side render the component or if it's not the first time it is instantiated, we fetch a
    fresh list of customers.
     */
    getCustomerIndexPage();
  }, [getCustomerIndexPage, ssrCustomers, stateCurrentPageNumber, stateCustomersPerPage, stateSearchValue]);

  useEffect(() => {
    /*
    Ensures search related states remain synced with URL search params in case the latter change because of
    navigation (e.g. back/forward or click on the current page's link in the navbar).
     */
    ReactRouterDomHelper.syncStatesWithSearchParams(
      {
        itemsPerPage: {
          defaultValue: defaultStatesValues.customersPerPage,
          setStateFunction: setStateCustomersPerPage
        },
        page: {
          defaultValue: defaultStatesValues.currentPageNumber,
          setStateFunction: setStateCurrentPageNumber
        },
        search: {
          defaultValue: defaultStatesValues.searchValue,
          setStateFunction: setStateSearchValue
        }
      },
      searchParams
    );
  }, [searchParams]);

  const updateCustomersPerPage = useCallback((newStateCustomersPerPage: number | string) => {
    if (typeof newStateCustomersPerPage === 'string') {
      newStateCustomersPerPage = Number(newStateCustomersPerPage);
    }

    setStateCurrentPageNumber(1);
    setStateCustomersPerPage(newStateCustomersPerPage);
    ReactRouterDomHelper.updateSearchParams(
      {
        itemsPerPage: String(newStateCustomersPerPage),
        page: '1'
      },
      searchParams,
      setSearchParams
    );
  }, [searchParams, setSearchParams]);

  const handlePageChange = useCallback((pageNumber: number) => {
    setStateCurrentPageNumber(pageNumber);
    ReactRouterDomHelper.updateSearchParams({page: String(pageNumber)}, searchParams, setSearchParams);
  }, [searchParams, setSearchParams]);

  const handleDelete = useCallback((customerId: number) => {
    return new Promise<void>((resolve, reject) => {
      deleteOneById(String(customerId))
        .then(() => {
          /*
           Will prevent empty page by refreshing the list and triggers a re-rendering of Paginator to
           update the pagination items if necessary.
           */
          getCustomerIndexPage();

          resolve();
        })
        .catch(() => {
          reject();
        })
    })
  }, [getCustomerIndexPage]);

  const handleSearchSubmit = useCallback((searchValue: string) => {
    setStateSearchValue(searchValue);

    let newCurrentPageNumber = stateCurrentPageNumber;

    /*
    If the search value has not changed since the last submit we need to call getCustomerIndexPage() manually as
    useEffect() (which usually calls getCustomerIndexPage()) won't be triggered.
     */
    if (searchValue === stateSearchValue) {
      getCustomerIndexPage();
    } else {
      /*
      User is not attempting to refresh the results of his previous search but is doing a new search, so we should
      go back to page 1.
       */
      newCurrentPageNumber = 1;
    }

    setStateCurrentPageNumber(newCurrentPageNumber);

    ReactRouterDomHelper.updateSearchParams(
      {
        page: String(newCurrentPageNumber),
        search: searchValue
      },
      searchParams,
      setSearchParams
    );
  }, [getCustomerIndexPage, searchParams, setSearchParams, stateCurrentPageNumber, stateSearchValue]);

  return (
    <>
      <SearchBar
        defaultValue={stateSearchValue}
        disabled={stateIsLoading}
        onSubmit={handleSearchSubmit}
        placeholder="Search by first name, last name or company"
      />
      <table className="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Company</th>
            <th>Invoices</th>
            <th>Unpaid</th>
            <th>Paid</th>
            <th>
              <DropdownButton
                block
                callback={updateCustomersPerPage}
                choices={useMemo(() => ['5', '10', '25'], [])}
                label={String(stateCustomersPerPage)}
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {stateError && (
            <tr>
              <td>{stateError}</td>
            </tr>
          )}
          {/*
          If stateCustomers.length === 0 && stateTotalCustomersCount !== 0 it means that the API has results
          but getCustomerIndexPage() has not had the time to update stateCustomers yet, so we assume the data
          is still loading.
           */}
          {(stateIsLoading || stateCustomers.length === 0) && stateTotalCustomersCount !== 0 && (
            <tr>
              <td>Loading...</td>
            </tr>
          )}
          {!stateIsLoading && !stateError && stateTotalCustomersCount === 0 && stateSearchValue !== '' && (
            <tr>
              <td>No results for &quot;{stateSearchValue}&quot;</td>
            </tr>
          )}
          {!stateIsLoading && !stateError && stateTotalCustomersCount === 0 && stateSearchValue === '' && (
            <tr>
              <td>No results</td>
            </tr>
          )}
          {!stateIsLoading && stateCustomers.map((customer: CustomerListCustomer) => {
            return (
              <CustomerItem key={customer.id} customer={customer} handleDeleteCallback={handleDelete}/>
            );
          })}
        </tbody>
      </table>
      <Paginator
        itemsPerPage={stateCustomersPerPage} totalItemsCount={stateTotalCustomersCount}
        currentPageNumber={stateCurrentPageNumber} setCurrentPageNumber={handlePageChange}
      />
    </>
  );
};

export {
  CustomersList
};
