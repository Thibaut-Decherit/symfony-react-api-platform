import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {Navbar} from './Navbar';

describe('Navbar', () => {
  test('basic rendering', () => {
    render(<Navbar/>);
    expect(screen.getByText('SymReact')).toBeVisible();
  });
});
