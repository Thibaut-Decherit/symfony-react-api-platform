import {DropdownButton} from '@/components/DropdownButton';
import {Paginator} from '@/components/Paginator';
import {SearchBar} from '@/components/SearchBar';
import {GlobalContext} from '@/GlobalContextProvider';
import {ReactRouterDomHelper} from '@/helpers/ReactRouterDomHelper';
import {InvoiceItem} from '@/pages/InvoiceIndexPage/InvoicesList/InvoiceItem';
import type {InvoiceListInvoice} from '@/pages/InvoiceIndexPage/InvoicesList/InvoicesList.types';
import {deleteOneById, paginatedFindByNameOrCompanyStartsBy} from '@/services/APIService/InvoiceAPIService';
import _ from 'lodash';
import React, {useCallback, useContext, useEffect, useMemo, useReducer, useState} from 'react';
import {useSearchParams} from 'react-router-dom';

/*
Declared outside the component to preserve the value between the component's instances. Must be set to true in a
useEffect() after first client side render.
 */
let ignoreSsrProps = false;

const defaultStatesValues = {
  currentPageNumber: 1,
  invoicesPerPage: 5,
  searchValue: ''
};

const reducerStateCurrentPageNumber = (
  state: number,
  action: {
    data?: number | string | null;
    type: 'update';
  }
): number => {
  switch (action.type) {
    case 'update': {
      let newData = action?.data;
      if (typeof newData === 'string') {
        newData = Number(newData);
      } else if (newData === null || newData === undefined) {
        newData = defaultStatesValues.currentPageNumber
      }

      return newData;
    }

    default:
      throw Error('Unknown action.');
  }
};

const reducerStateInvoicesPerPage = (
  state: number,
  action: {
    data: number | string | null;
    type: 'update';
  }
): number => {
  switch (action.type) {
    case 'update': {
      let newData = action?.data;
      if (typeof newData === 'string') {
        newData = Number(newData);
      } else if (newData === null || newData === undefined) {
        newData = defaultStatesValues.invoicesPerPage
      }

      return newData;
    }
  }
  throw Error('Unknown action.');
};

const reducerStateSearchValue = (
  state: string,
  action: {
    data?: number | string | null;
    type: 'update';
  }
): string => {
  switch (action.type) {
    case 'update': {
      let newData = action?.data;
      if (typeof newData === 'number') {
        newData = String(newData);
      } else if (newData === null || newData === undefined) {
        newData = defaultStatesValues.searchValue
      }

      return newData;
    }

    default:
      throw Error('Unknown action.');
  }
};

const InvoicesList = () => {
  const globalContext = useContext(GlobalContext);
  const ssrProps = globalContext.routeSsrProps?.InvoiceIndexPage?.InvoicesList;
  const ssrInvoices: {
    results?: InvoiceListInvoice[],
    totalItemsCount?: number
  } | undefined = ssrProps?.invoices;
  const [searchParams, setSearchParams] = useSearchParams();

  const [stateCurrentPageNumber, dispatchStateCurrentPageNumber] = useReducer(
    reducerStateCurrentPageNumber,
    !ignoreSsrProps
      ? Number(_.get(ssrProps, 'searchParams.page', defaultStatesValues.currentPageNumber))
      // Always returns null during SSR, so we need to pass it via ssrProps, or we will always get the default value.
      : Number(_.defaultTo(searchParams.get('page'), defaultStatesValues.currentPageNumber))
  );
  const setStateCurrentPageNumber = (newState: number | string | null) => {
    dispatchStateCurrentPageNumber({data: newState, type: 'update'});
  };
  const [stateInvoices, setStateInvoices] = useState(
    !ignoreSsrProps
      ? ssrInvoices?.results || []
      : []
  );
  const [stateInvoicesPerPage, dispatchStateInvoicesPerPage] = useReducer(
    reducerStateInvoicesPerPage,
    !ignoreSsrProps
      ? Number(_.get(ssrProps, 'searchParams.itemsPerPage', defaultStatesValues.invoicesPerPage))
      // Always returns null during SSR, so we need to pass it via ssrProps, or we will always get the default value.
      : Number(_.defaultTo(searchParams.get('itemsPerPage'), defaultStatesValues.invoicesPerPage))
  );
  const setStateInvoicesPerPage = (newState: number | string | null) => {
    dispatchStateInvoicesPerPage({data: newState, type: 'update'});
  };
  const [stateError, setStateError] = useState<boolean | string>(false);

  /*
  If !ignoreSsrProps we may have to populate the list with data from ssrInvoices (coming from the server in
  props passed during SSR), so no need to show the loading message until data is fetched: it's already available in
  ssrInvoices).
  Except if ssrInvoices is undefined, then it means the component has been accessed via react-router-dom, so
  we need to show the loading message because we have data to fetch.
   */
  const [stateIsLoading, setStateIsLoading] = useState(
    !ignoreSsrProps
      ? ssrInvoices === undefined
      : true
  );

  const [stateSearchValue, dispatchStateSearchValue] = useReducer(
    reducerStateSearchValue,
    !ignoreSsrProps
      ? _.get(ssrProps, 'searchParams.search', defaultStatesValues.searchValue)
      // Always returns null during SSR, so we need to pass it via ssrProps, or we will always get the default value.
      : _.defaultTo(searchParams.get('search'), defaultStatesValues.searchValue)
  );
  const setStateSearchValue = (newState: number | string | null) => {
    dispatchStateSearchValue({data: newState, type: 'update'});
  };
  const [stateTotalInvoicesCount, setStateTotalInvoicesCount] = useState(
    !ignoreSsrProps
      ? ssrInvoices?.totalItemsCount || 0
      : 0
  );

  const getInvoiceIndexPage = useCallback(() => {
    setStateError(false);
    setStateIsLoading(true);

    paginatedFindByNameOrCompanyStartsBy(
      stateInvoicesPerPage,
      stateCurrentPageNumber,
      stateSearchValue
    )
      .then(
        (response: {
          results: InvoiceListInvoice[],
          totalItemsCount: number
        }) => {
          setStateInvoices(response.results);
          setStateTotalInvoicesCount(response.totalItemsCount);
        })
      .catch((error: Error) => {
        setStateInvoices([]);
        setStateTotalInvoicesCount(0);

        if (error.message === 'Network Error') {
          setStateError('Network error');
        } else {
          setStateError('Something went wrong');
        }
      })
      .finally(() => {
        setStateIsLoading(false);
      })
  }, [stateCurrentPageNumber, stateInvoicesPerPage, stateSearchValue]);

  /*
   useEffect hook is called on render.
   If optional second parameter is:
      - not specified, the hook is called on each render
      - an empty array, the hook is called only on the first render of the component
      - an array containing one or multiple variables (most probably states), the hook is called on render if one of
      the values has changed since previous render
   See https://reactjs.org/docs/hooks-effect.html
   */
  useEffect(() => {
    if (!ignoreSsrProps) {
      ignoreSsrProps = true;

      /*
      If we just server side rendered the component (ssrProps.invoices is defined) the server already passed a
      list of invoices via props, so we don't need to fetch a new one until the user interacts with the
      paginator, the items per page dropdown or the search bar.
       */
      if (ssrInvoices !== undefined) {
        return;
      }
    }

    getInvoiceIndexPage();
  }, [getInvoiceIndexPage, ssrInvoices, stateCurrentPageNumber, stateInvoicesPerPage, stateSearchValue]);

  useEffect(() => {
    /*
    Ensures search related states remain synced with URL search params in case the latter change because of
    navigation (e.g. back/forward or click on the current page's link in the navbar).
     */
    ReactRouterDomHelper.syncStatesWithSearchParams(
      {
        itemsPerPage: {
          defaultValue: defaultStatesValues.invoicesPerPage,
          setStateFunction: setStateInvoicesPerPage
        },
        page: {
          defaultValue: defaultStatesValues.currentPageNumber,
          setStateFunction: setStateCurrentPageNumber
        },
        search: {
          defaultValue: defaultStatesValues.searchValue,
          setStateFunction: setStateSearchValue
        }
      },
      searchParams
    );
  }, [searchParams]);

  const updateInvoicesPerPage = useCallback((newStateInvoicesPerPage: number | string) => {
    setStateCurrentPageNumber(1);
    setStateInvoicesPerPage(newStateInvoicesPerPage);
    ReactRouterDomHelper.updateSearchParams(
      {
        itemsPerPage: String(newStateInvoicesPerPage),
        page: '1'
      },
      searchParams,
      setSearchParams
    );
  }, [searchParams, setSearchParams]);

  const handlePageChange = useCallback((pageNumber: number) => {
    setStateCurrentPageNumber(pageNumber);
    ReactRouterDomHelper.updateSearchParams({page: String(pageNumber)}, searchParams, setSearchParams);
  }, [searchParams, setSearchParams]);

  const handleDelete = useCallback((invoiceId: number) => {
    return new Promise<void>((resolve, reject) => {
      deleteOneById(String(invoiceId))
        .then(() => {
          /*
           Will prevent empty page by refreshing the list and triggers a re-rendering of Paginator to
           update the pagination items if necessary.
           */
          getInvoiceIndexPage();

          resolve();
        })
        .catch(() => {
          reject();
        })
    })
  }, [getInvoiceIndexPage]);

  const handleSearchSubmit = useCallback((searchValue: string) => {
    setStateSearchValue(searchValue);

    let newCurrentPageNumber = stateCurrentPageNumber;

    /*
    If the search value has not changed since the last submit we need to call getInvoiceIndexPage() manually as
    useEffect() (which usually calls getInvoiceIndexPage()) won't be triggered.
     */
    if (searchValue === stateSearchValue) {
      getInvoiceIndexPage();
    } else {
      /*
      User is not attempting to refresh the results of his previous search but is doing a new search, so we should
      go back to page 1.
       */
      newCurrentPageNumber = 1;
    }

    setStateCurrentPageNumber(newCurrentPageNumber);

    ReactRouterDomHelper.updateSearchParams(
      {
        page: String(newCurrentPageNumber),
        search: searchValue
      },
      searchParams,
      setSearchParams
    );
  }, [getInvoiceIndexPage, searchParams, setSearchParams, stateCurrentPageNumber, stateSearchValue]);

  return (
    <>
      <SearchBar
        defaultValue={stateSearchValue}
        disabled={stateIsLoading}
        onSubmit={handleSearchSubmit}
        placeholder="Search by first name, last name or company"
      />
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Nbr.</th>
            <th>Customer</th>
            <th>Email</th>
            <th>Company</th>
            <th className="text-center">Amount</th>
            <th className="text-center">Emitted at</th>
            <th className="text-center">Status</th>
            <th>
              <DropdownButton
                block
                callback={updateInvoicesPerPage}
                choices={useMemo(() => ['5', '10', '25'], [])}
                label={String(stateInvoicesPerPage)}
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {stateError && (
            <tr>
              <td>{stateError}</td>
            </tr>
          )}
          {/*
          If stateInvoices.length === 0 && stateTotalInvoicesCount !== 0 it means that the API has results
          but getInvoiceIndexPage() has not had the time to update stateInvoices yet, so we assume the data is
          still loading.
           */}
          {(stateIsLoading || stateInvoices.length === 0) && stateTotalInvoicesCount !== 0 && (
            <tr>
              <td>Loading...</td>
            </tr>
          )}
          {!stateIsLoading && !stateError && stateTotalInvoicesCount === 0 && stateSearchValue !== '' && (
            <tr>
              <td>No results for &quot;{stateSearchValue}&quot;</td>
            </tr>
          )}
          {!stateIsLoading && !stateError && stateTotalInvoicesCount === 0 && stateSearchValue === '' && (
            <tr>
              <td>No results</td>
            </tr>
          )}
          {!stateIsLoading && stateInvoices.map(invoice => {
            return (
              <InvoiceItem
                key={invoice.id} invoice={invoice} handleDeleteCallback={handleDelete}
              />
            );
          })}
        </tbody>
      </table>
      <Paginator
        itemsPerPage={stateInvoicesPerPage} totalItemsCount={stateTotalInvoicesCount}
        currentPageNumber={stateCurrentPageNumber} setCurrentPageNumber={handlePageChange}
      />
    </>
  );
};

export {
  InvoicesList
};
