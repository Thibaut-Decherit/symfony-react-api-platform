<?php

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;

/**
 * Class ReactAppRoutesGeneratorService
 *
 * Provides a method to generate an array of routes consumable by the React app via axios, jest and react-router-dom.
 *
 * @package App\Service
 */
class ReactAppRoutesGeneratorService
{
    private RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function generate(): array
    {
        $routes = array_filter(
            $this->router->getRouteCollection()->all(),
            function ($key) {
                if (!is_string($key)) {
                    return false;
                }

                return str_starts_with($key, 'react_app_');
            },
            ARRAY_FILTER_USE_KEY
        );

        /*
         * Builds an array of route_name => path while converting parameters syntax from {example} to :example, which is
         * the syntax supported by react-router-dom.
         */
        foreach ($routes as $name => $route) {
            $routes[$name] = str_replace(
                ['{', '}'],
                [':', ''],
                $route->getPath()
            );
        }

        return $routes;
    }
}
