import {DeleteButton} from '@/components/DeleteButton';
import type {CustomerListCustomer} from '@/pages/CustomerIndexPage/CustomersList/CustomersList.types';
import PropTypes from 'prop-types';
import React, {useCallback} from 'react';
import {Link} from 'react-router-dom';

const CustomerItem = (
  {
    customer,
    handleDeleteCallback
  }: {
    customer: CustomerListCustomer;
    handleDeleteCallback: (customerId: number) => Promise<void>;
  }
) => {
  /*
  async/await seems to be needed, else deleting an item throws the "handleDelete() is not a function" error when it is
  time to call .then(), .catch() or .finally() on it (see inside assets/js/components/DeleteButton.jsx
  handleDeleteButtonClick() function). This does not occur without useCallback() or if the promise returned by
  handleDeleteButtonClick() is ignored.
   */
  const handleDelete = useCallback(async () => {
    await handleDeleteCallback(customer.id);
  }, [customer.id, handleDeleteCallback]);

  return (
    <tr>
      <td>{customer.id}</td>
      <td>
        <Link to={`/customer/${customer.id}`}>{customer.firstName + ' ' + customer.lastName}</Link>
      </td>
      <td>
        <a href={`mailto:${customer.email}`}>{customer.email}</a>
      </td>
      <td>{customer.company || 'n/a'}</td>
      <td>{customer.invoices.length}</td>
      <td>{parseFloat(customer.unpaidAmount).toLocaleString()} €</td>
      <td>{parseFloat(customer.paidAmount).toLocaleString()} €</td>
      <td>
        <DeleteButton block disabled={customer.invoices.length > 0} handleDelete={handleDelete}/>
      </td>
    </tr>
  );
};

CustomerItem.propTypes = {
  customer: PropTypes.shape({
    company: PropTypes.string,
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    invoices: PropTypes.array.isRequired,
    lastName: PropTypes.string.isRequired,
    paidAmount: PropTypes.string.isRequired,
    unpaidAmount: PropTypes.string.isRequired
  }).isRequired,
  handleDeleteCallback: PropTypes.func.isRequired
};

export {
  CustomerItem
};
