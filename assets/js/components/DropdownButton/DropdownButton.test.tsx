import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {DropdownButton} from './DropdownButton';

describe('DropdownButton', () => {
  test('basic rendering', () => {
    render(<DropdownButton callback={jest.fn()} choices={['test']}/>);
    expect(screen.getByRole('button')).toBeVisible();
  });
});
