export type CustomerListCustomer = {
  company: string | null;
  email: string;
  firstName: string;
  id: number;
  invoices: object[];
  lastName: string;
  paidAmount: string;
  unpaidAmount: string;
};
