const Encore = require('@symfony/webpack-encore');
const path = require("path");

Encore
    .setOutputPath('var/webpack/')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/')

    // Purges the outputPath directory before each build (doesn't work on subsequent builds triggered by --watch).
    .cleanupOutputBeforeBuild()

    .enableReactPreset()

    // so we don't need to deal with runtime.js
    .disableSingleRuntimeChunk()

    // will output as var/webpack/server-bundle.js
    .addEntry('server-bundle', './assets/js/serverSideEntryPoint.ts')

    // Allows sass/scss files to be processed.
    .enableSassLoader()

    // Uncomment if you use TypeScript.
    .enableTypeScriptLoader(function (options) {
        options.configFile = Encore.isProduction() ? 'tsconfig.prod.json' : 'tsconfig.json';
    })

    .addAliases({
        '@': path.resolve(__dirname, 'assets/js')
    })

// Shows OS notifications when builds finish/fail.
// .enableBuildNotifications()
;

module.exports = Encore.getWebpackConfig();
