<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\CustomerNameOrCompanyFilter;
use App\Singleton\MoneySingleton;
use Closure;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Money\Currency;
use Money\Money;
use Money\Parser\DecimalMoneyParser;
use Symfony\Component\Serializer\Annotation\Groups as SerializerGroups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 * @ApiResource(
 *     itemOperations={
 *          "delete"={
 *              "security"="is_granted('CUSTOMER_DELETE', object)"
 *          },
 *          "get",
 *          "patch",
 *          "put"
 *     },
 *     normalizationContext={
 *          "groups"={"customer_read"}
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "firstName": "start",
 *     "lastName": "start",
 *     "company": "start"
 * })
 * @ApiFilter(OrderFilter::class)
 * @ApiFilter(CustomerNameOrCompanyFilter::class, strategy="start")
 */
class Customer
{
    /**
     * Do not remove, it is used dynamically in
     * App\Controller\ReactApp\AbstractReactAppController::cleanupSearchParams().
     *
     * @var array
     */
    const DEFAULT_SEARCH_PARAMS = [
        'itemsPerPage' => 5,
        'page' => 1
    ];

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @SerializerGroups({"customer_read", "invoice_read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @SerializerGroups({"customer_read", "invoice_read"})
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "form_errors.global.min_length",
     *      maxMessage = "form_errors.global.max_length",
     * )
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @SerializerGroups({"customer_read", "invoice_read"})
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "form_errors.global.min_length",
     *      maxMessage = "form_errors.global.max_length",
     * )
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @SerializerGroups({"customer_read", "invoice_read"})
     * @Assert\Email(message = "form_errors.user.valid_email")
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "form_errors.global.min_length",
     *      maxMessage = "form_errors.global.max_length",
     * )
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializerGroups({"customer_read", "invoice_read"})
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "form_errors.global.min_length",
     *      maxMessage = "form_errors.global.max_length",
     * )
     */
    private $company;

    /**
     * @var Collection|Invoice[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Invoice", mappedBy="customer", orphanRemoval=true)
     * @SerializerGroups({"customer_read"})
     * @ApiSubresource()
     */
    private $invoices;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="customers")
     * @ORM\JoinColumn(nullable=false)
     * @SerializerGroups({"customer_read"})
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $user;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }

    /**
     * @SerializerGroups({"customer_read"})
     * @return string
     */
    public function getPaidAmount(): string
    {
        return $this->sumAmount(
            function (Invoice $invoice, DecimalMoneyParser $parser, Currency $currency) {
                if ($invoice->getStatus() === 'PAID') {
                    return $parser->parse($invoice->getAmount(), $currency);
                }

                return Money::EUR(0);
            }
        );
    }

    /**
     * @SerializerGroups({"customer_read"})
     * @return string
     */
    public function getTotalAmount(): string
    {
        return $this->sumAmount(
            function (Invoice $invoice, DecimalMoneyParser $parser, Currency $currency) {
                return $parser->parse($invoice->getAmount(), $currency);
            }
        );
    }

    /**
     * @SerializerGroups({"customer_read"})
     * @return string
     */
    public function getUnpaidAmount(): string
    {
        return $this->sumAmount(
            function (Invoice $invoice, DecimalMoneyParser $parser, Currency $currency) {
                if ($invoice->getStatus() === 'SENT') {
                    return $parser->parse($invoice->getAmount(), $currency);
                }

                return Money::EUR(0);
            }
        );
    }

    /**
     * @param Closure(Invoice, DecimalMoneyParser, Currency): Money $getAmountToAdd
     *
     * @return string
     */
    private function sumAmount(Closure $getAmountToAdd): string
    {
        $moneySingleton = MoneySingleton::getInstance();
        $parser = $moneySingleton->getDecimalMoneyParser();
        $moneyFormatter = $moneySingleton->getDecimalMoneyFormatter();
        $currency = $moneySingleton->getCurrency('EUR');
        $total = Money::EUR(0);

        /**
         * @var Invoice $invoice
         */
        foreach ($this->invoices->toArray() as $invoice) {
            $total = $total->add(
                $getAmountToAdd($invoice, $parser, $currency)
            );
        }

        return $moneyFormatter->format($total);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return $this->company;
    }

    /**
     * @param string|null $company
     * @return $this
     */
    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    /**
     * @param Invoice $invoice
     * @return $this
     */
    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setCustomer($this);
        }

        return $this;
    }

    /**
     * @param Invoice $invoice
     * @return $this
     */
    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getCustomer() === $this) {
                $invoice->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
