import _ from 'lodash';
import {Dispatch, SetStateAction} from 'react';
import {SetURLSearchParams} from 'react-router-dom';
import {URLSearchParamsInit} from 'react-router-dom/dist/dom';

class ReactRouterDomHelper {
  /**
   * Allow search related states specified in the config parameter to remain synced with URL search params in case the
   * latter change because of navigation (e.g. back/forward or click on the current page's link in the navbar).
   */
  static syncStatesWithSearchParams(
    config: {
      [key: string]: {
        defaultValue: number | string | null;
        /*
        The first supported type is for proper setState() functions from useState(), the second one is for 'fake'
        setState() functions wrapping useReducer() dispatch().
        Use the first one if your state supports number | string | null types.
        If your state only supports string or number, use the second one with a reducer supporting
        number | string | null but enforcing your state type by casting the 'fake' setState() parameter to the expected
        type (number or string), or by assigning a default value of the expected type if the parameter of the 'fake'
        setState() is null.
         */
        setStateFunction: Dispatch<SetStateAction<number | string | null>> | ((newState: number | string | null) => void);
      }
    },
    searchParams: URLSearchParams
  ) {
    Object.keys(config).forEach(searchParamName => {
      const searchParam: number | string | null = searchParams.get(searchParamName);
      if (searchParam !== null) {
        // Search param is still in the URL, so we ensure the related state has the same value.
        config[searchParamName].setStateFunction(searchParam);
      } else {
        // Search param is no longer in the URL, so we set the related state back to its default value.
        config[searchParamName].setStateFunction(config[searchParamName].defaultValue);
      }
    });
  }

  static updateSearchParams(
    data: {
      [key: string]: string
    },
    searchParams: URLSearchParams,
    setSearchParams: SetURLSearchParams
  ) {
    setSearchParams(_.assign<URLSearchParamsInit, object>(Object.fromEntries(searchParams), _.cloneDeep(data)));
  }
}

export {
  ReactRouterDomHelper
};
