<?php

namespace App\Controller\ReactApp;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractReactAppController
{
    /**
     * @param Request $request
     * @Route(name="react_app_home_page")
     * @return Response
     */
    public function home(Request $request): Response
    {
        return $this->render('app.html.twig', [
            'props' => $this->buildProps($request),
            'title' => $this->buildDocumentTitle()
        ]);
    }
}
