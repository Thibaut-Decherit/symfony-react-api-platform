import type {CustomerPageCustomer} from '@/pages/CustomerPage/CustomerPage.types';
import {AxiosResponse} from 'axios';

export interface AxiosResponseWithCustomerPageCustomer extends AxiosResponse {
  data: CustomerPageCustomer
}
