import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {DeleteButton} from './DeleteButton';

describe('DeleteButton', () => {
  test('basic rendering', () => {
    render(<DeleteButton handleDelete={jest.fn()}/>);
    expect(screen.getByRole('button')).toBeVisible();
  });
});
