import type {CustomerListCustomer} from '@/pages/CustomerIndexPage/CustomersList/CustomersList.types';
import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {CustomerItem} from './CustomerItem';

const customer: CustomerListCustomer = {
  company: null,
  email: 'test@test.test',
  firstName: 'test',
  id: 1,
  invoices: [],
  lastName: 'test',
  paidAmount: '1',
  unpaidAmount: '1'
};
const handleDeleteCallback = jest.fn();

describe('CustomerItem', () => {
  test('basic rendering', () => {
    render(
      <table>
        <tbody>
          <CustomerItem customer={customer} handleDeleteCallback={handleDeleteCallback}/>
        </tbody>
      </table>
    );
    expect(screen.getByRole('row')).toBeVisible();
  });
});
