<?php

namespace App\EventListener\ResponseHeaderSetter\DynamicResponseHeaderSetter;

use App\Helper\StringHelper;
use App\Service\SessionTokenService;
use Exception;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class CspHeaderSetter
 *
 * Adds Content Security Policy header to a response.
 * See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
 *
 * @package App\EventListener\ResponseHeaderSetter\DynamicResponseHeaderSetter
 */
class CspHeaderSetter
{
    /**
     * @var string
     */
    private string $kernelEnvironment;

    /**
     * @var RequestStack
     */
    private RequestStack $requestStack;

    /**
     * @var ResponseHeaderBag
     */
    private ResponseHeaderBag $responseHeaders;

    /**
     * @var array
     */
    private array $cspConfig;

    /**
     * @var SessionTokenService
     */
    private SessionTokenService $sessionTokenService;

    /**
     * @var RouterInterface
     */
    private RouterInterface $router;

    /**
     * @var array
     */
    private array $directives;

    /**
     * CspHeaderSetter constructor
     *
     * @param string $kernelEnvironment
     * @param RequestStack $requestStack
     * @param ResponseHeaderBag $responseHeaders
     * @param array $cspConfig
     * @param SessionTokenService $sessionTokenService
     * @param RouterInterface $router
     */
    public function __construct(
        string $kernelEnvironment,
        RequestStack $requestStack,
        ResponseHeaderBag $responseHeaders,
        array $cspConfig,
        SessionTokenService $sessionTokenService,
        RouterInterface $router
    )
    {
        $this->kernelEnvironment = $kernelEnvironment;
        $this->requestStack = $requestStack;
        $this->responseHeaders = $responseHeaders;
        $this->cspConfig = $cspConfig;
        $this->sessionTokenService = $sessionTokenService;
        $this->router = $router;
        $this->directives = [];
    }

    /**
     * @throws Exception
     */
    public function set(): void
    {
        $this->responseHeaders->set('Content-Security-Policy', $this->generate());
    }

    /**
     * Generates Content Security Policy header value.
     * See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
     * @return string
     * @throws Exception
     * @throws InvalidArgumentException
     */
    private function generate(): string
    {
        $this->parseDirectivesConfig();

        $this->addReportUri();

        $this->addDevDirectivesIfDevEnvironment();

        $headerValue = '';

        foreach ($this->getDirectives() as $directiveName => $directiveContent) {
            if (empty($directiveContent)) {
                throw new InvalidArgumentException("$directiveName: Directives cannot be empty");
            } elseif (!is_array($directiveContent)) {
                throw new InvalidArgumentException("$directiveName: Directives must be of type array");
            } elseif (in_array(null, $directiveContent) || in_array("", $directiveContent)) {
                throw new InvalidArgumentException("$directiveName: Directives cannot contain null or empty values");
            }

            $directiveContentString = '';

            foreach ($directiveContent as $key => $directiveSource) {
                // Generates nonce according to $directiveSource if necessary.
                if (StringHelper::startsWith($directiveSource, 'CSP-nonce/')) {
                    $nonceKey = $directiveSource;
                    $nonce = $this->sessionTokenService->get($nonceKey);

                    $directiveContent[$key] = "'nonce-$nonce'";

                    /*
                     * We have to refresh the nonce on every request or it would not be a proper nonce.
                     */
                    $this->sessionTokenService->refresh($nonceKey);
                }

                $directiveContentString .= ' ' . $directiveContent[$key];
            }

            $directiveContentString = trim($directiveContentString);

            $directive = "$directiveName $directiveContentString; ";

            $headerValue .= $directive;
        }

        return $headerValue;
    }

    /**
     * Sets $this->directives with base directive and potential overrides if current path matches.
     * @throws InvalidArgumentException
     */
    private function parseDirectivesConfig(): void
    {
        if (empty($this->cspConfig['directives']['base'])) {
            throw new InvalidArgumentException(
                'app.content_security_policy.directives.base: At least one base directive must be defined'
            );
        }

        $matchingOverride = [];

        if (!empty($this->cspConfig['directives']['overrides'])) {
            $pathInfo = $this->requestStack->getMasterRequest()->getPathInfo();

            // Parses overrides to find a path matching current path ($pathInfo).
            foreach ($this->cspConfig['directives']['overrides'] as $key => $override) {
                if (!is_array($override['paths']) || empty($override['paths'])) {
                    throw new InvalidArgumentException(
                        "app.content_security_policy.directives.overrides.paths ($key): paths must be an array and not empty"
                    );
                }

                foreach ($override['paths'] as $path) {
                    $path = str_replace('/', '\/', $path);
                    if (preg_match("/$path/", $pathInfo)) {
                        if (
                            !is_array($this->cspConfig['directives']['overrides'][$key]['directives'])
                            && empty($this->cspConfig['directives']['overrides'][$key]['directives'])
                        ) {
                            throw new InvalidArgumentException(
                                "app.content_security_policy.directives.overrides ($key): directives must be an array and not empty"
                            );
                        }

                        $matchingOverride = $this->cspConfig['directives']['overrides'][$key]['directives'];

                        // A match has been found, no need to parse the remaining paths and overrides.
                        break 2;
                    }
                }
            }
        }

        $this->setDirectives(array_merge($this->cspConfig['directives']['base'], $matchingOverride));
    }

    /**
     * @throws InvalidArgumentException
     */
    private function addReportUri(): void
    {
        if (!isset($this->cspConfig['report_uri'])) {
            return;
        }

        if (empty($this->cspConfig['report_uri']['mode'])) {
            throw new InvalidArgumentException('app.content_security_policy.report_uri.mode is undefined or empty');
        } elseif (empty($this->cspConfig['report_uri']['data'])) {
            throw new InvalidArgumentException('app.content_security_policy.report_uri.data is undefined or empty');
        }

        $directivesArray = $this->getDirectives();

        $directivesArray['report-uri'][] = match ($this->cspConfig['report_uri']['mode']) {
            'plain' => $this->cspConfig['report_uri']['data'],
            'match' => $this->router->generate($this->cspConfig['report_uri']['data']),
            default => throw new InvalidArgumentException(
                "app.content_security_policy.report_uri.mode must be of type string and contain 'plain' or 'match'"
            )
        };

        $this->setDirectives($directivesArray);
    }

    /**
     * Adds dev only directives if app is running in dev environment.
     */
    private function addDevDirectivesIfDevEnvironment(): void
    {
        if ($this->kernelEnvironment !== 'dev') {
            return;
        }

        $directivesArray = $this->getDirectives();

        /*
         * In dev env 'self' === http://localhost:port, NOT 127.0.0.1. You need to whitelist this IP if you dev at
         * http://127.0.0.1:port and not at http://localhost:port.
         */
        $baseUrl = $this->requestStack->getMasterRequest()->getSchemeAndHttpHost();

        $scriptSrcDevDirectiveContent = [
            $baseUrl,
            "'unsafe-eval'",
            "'unsafe-inline'"
        ];

        $styleSrcDevDirectiveContent = [
            $baseUrl,
            "'unsafe-inline'"
        ];

        $directivesArray['connect-src'][] = $baseUrl;
        $directivesArray['font-src'][] = $baseUrl;
        $directivesArray['form-action'][] = $baseUrl;

        /*
         * Allows Symfony Profiler to work properly as it relies on inline JS and CSS.
         * array_unique() prevents CSP duplicate source (e.g. 'unsafe-inline' is already in your script-src policy)
         * error on some browsers (e.g. Firefox).
         */
        $directivesArray['script-src'] = array_unique(
            array_merge($directivesArray['script-src'], $scriptSrcDevDirectiveContent)
        );
        $directivesArray['style-src'] = array_unique(
            array_merge($directivesArray['style-src'], $styleSrcDevDirectiveContent)
        );

        $this->setDirectives($directivesArray);
    }

    /**
     * @return array
     */
    private function getDirectives(): array
    {
        return $this->directives;
    }

    /**
     * @param array $directives
     * @return CspHeaderSetter
     */
    private function setDirectives(array $directives): CspHeaderSetter
    {
        $this->directives = $directives;

        return $this;
    }
}
