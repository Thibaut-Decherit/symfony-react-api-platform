<?php

namespace App\Controller\ReactApp;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route(path="/customer")
 */
class CustomerController extends AbstractReactAppController
{
    /**
     * @param Request $request
     * @param Customer $customer
     * @param SerializerInterface $serializer
     * @Route(path="/{id}", name="react_app_customer_edit_page", requirements={"id"="\d+"})
     * @return Response
     */
    public function edit(Request $request, Customer $customer, SerializerInterface $serializer): Response
    {
        return $this->render('app.html.twig', [
            'props' => $this->buildProps(
                $request,
                [
                    'CustomerPage' => [
                        /*
                         * serialize() applies serialization groups to limit exposed data and converts entities to JSON.
                         * Then we call json_decode() because react_component() Twig function expects objects and
                         * handles the JSON encoding itself.
                         */
                        'customer' => json_decode($serializer->serialize($customer, 'jsonld'))
                    ]
                ]
            ),
            'title' => $this->buildDocumentTitle('Edit customer')
        ]);
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param CustomerRepository $customerRepository
     * @Route(path="/index", name="react_app_customer_index_page")
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function index(
        Request $request,
        SerializerInterface $serializer,
        CustomerRepository $customerRepository
    ): Response
    {
        $searchParams = $this->cleanupSearchParams(Customer::class, $request->query->all());

        if ($searchParams['has_errors']) {
            return $this->redirectToRoute('react_app_customer_index_page', $searchParams['search_params']);
        }

        $customers = $customerRepository->reactAppIndexSearch($searchParams['search_params'], $this->getUser());

        return $this->render('app.html.twig', [
            'props' => $this->buildProps(
                $request,
                [
                    'CustomerIndexPage' => [
                        'CustomersList' => [
                            /*
                             * serialize() applies serialization groups to limit exposed data and converts entities to
                             * JSON. Then we call json_decode() because react_component() Twig function expects objects
                             * and handles the JSON encoding itself.
                             */
                            'customers' => json_decode($serializer->serialize($customers, 'jsonld')),
                            'searchParams' => $searchParams['search_params']
                        ]
                    ]
                ]
            ),
            'title' => $this->buildDocumentTitle('Customers')
        ]);
    }

    /**
     * @param Request $request
     * @Route(path="/new", name="react_app_customer_new_page")
     * @return Response
     */
    public function new(Request $request): Response
    {
        return $this->render('app.html.twig', [
            'props' => $this->buildProps($request),
            'title' => $this->buildDocumentTitle('Add customer')
        ]);
    }
}
