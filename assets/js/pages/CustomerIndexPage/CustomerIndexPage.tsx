import {GlobalContext} from '@/GlobalContextProvider';
import {CustomersList} from '@/pages/CustomerIndexPage/CustomersList/CustomersList';
import {setTitle} from '@/services/DocumentService';
import React, {useContext} from 'react';
import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

const CustomerIndexPage = () => {
  setTitle('Customers');

  const globalContext = useContext(GlobalContext);

  return (
    <>
      <div className="mb-2 d-flex justify-content-between">
        <h1 className="mb-0">Customers</h1>
        <div className="d-flex justify-content-end align-items-center">
          <Link to={globalContext.ssrRoutes.react_app_customer_new_page}>
            <Button variant="success" size="lg">
              Add
            </Button>
          </Link>
        </div>
      </div>
      <CustomersList/>
    </>
  );
};

export {
  CustomerIndexPage
};
