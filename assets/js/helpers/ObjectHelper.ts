import _ from 'lodash';

class ObjectHelper {

  /**
   * For each key of object, attempts to overwrite its value with the value of source at the same key if source has said
   * key.
   *
   * @param {T} object
   * @param {Record<string, any>} source
   */
  static updateSharedKeys<T extends Record<string, any>>(object: T, source: Record<string, any>) {
    const newObject = {...object};
    for (const [key] of Object.entries(object)) {
      if (_.has(source, key)) {
        newObject[key as keyof T] = _.cloneDeep(source[key]);
      }
    }

    return newObject;
  }
}

export {
  ObjectHelper
};
