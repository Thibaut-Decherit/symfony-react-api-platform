// Provides information related to SSR and APIs not available during SSR.

/*
Called as functions to ensure data is accurate. Storing the result of these functions directly in constants appeared to
be done at least partially server side only, without getting 'updated' client side and thus not reflecting the reality
(still saying we are server side when called client side). Maybe because the constants were initialized server side and
passed as is client side without getting initialized 'again' there.
 */
const canUseDocument = () => {
  return typeof document !== 'undefined';
};
const canUseLocalStorage = () => {
  return typeof localStorage !== 'undefined';
};
const canUseWindow = () => {
  return typeof window !== 'undefined';
};
const isSsr = () => {
  return !canUseDocument() || !canUseLocalStorage() || !canUseWindow();
};

export {
  canUseDocument,
  canUseLocalStorage,
  canUseWindow,
  isSsr
};
