<?php

namespace App\Security\Voter;

use App\Entity\Invoice;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class InvoiceVoter extends Voter
{
    const ATTRIBUTE_INVOICE_DELETE = 'INVOICE_DELETE';
    const SUPPORTED_ATTRIBUTES = [
        self::ATTRIBUTE_INVOICE_DELETE
    ];
    const SUPPORTED_SUBJECTS = [
        Invoice::class
    ];

    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, self::SUPPORTED_ATTRIBUTES)) {
            return false;
        }

        if (!is_object($subject) || !in_array(get_class($subject), self::SUPPORTED_SUBJECTS)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param Invoice $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::ATTRIBUTE_INVOICE_DELETE:
                if ($subject->getUser() === $token->getUser() && $subject->getStatus() === Invoice::STATUS_CANCELLED) {
                    return true;
                }
                break;
            default:
                throw new LogicException('$attribute ' . $attribute . ' is not supported.');
        }

        return false;
    }
}
