import {canUseWindow} from '@/helpers/SsrHelper';

class URLHelper {
  /**
   * @param {string} path
   *
   * @param {?string} origin
   * If undefined, defaults to document's origin.
   *
   * @return {string|URL}
   */
  static buildAbsolute(path: string, origin?: string) {
    /*
    Turns out path is already an absolute URL, so we extract its path, in case the function is called to change the
    origin.
    Warning: Do NOT rework that behavior in an existing app (e.g. if path is already an absolue URL then return it
    without modifying anything) or the current behavior (default to document's origin) could render features relying on
    it vulnerable to open redirects.
     */
    if (this.isValid(path)) {
      path = new URL(path).pathname;
    }

    // Defaults to document's origin.
    if (origin === undefined) {
      origin = this.getOrigin();
    }

    return origin + path;
  }

  /**
   * @param {?string} url
   * If undefined or invalid, defaults to current page.
   *
   * @param {boolean} removePort
   * @return {string}
   */
  static getOrigin(url?: string, removePort = false) {
    if (!canUseWindow()) {
      return '';
    }

    if (url === undefined || !this.isValid(url)) {
      url = window.location.href;
    }

    let origin = new URL(url).origin;

    if (removePort) {
      origin = origin.replace(/:\d+$/, '');
    }

    return origin;
  }

  static isSameOrigin(url?: string, ignorePort = false) {
    if (typeof url !== 'string') {
      return false;
    }

    if (!this.isValid(url)) {
      return false;
    }

    return this.getOrigin(undefined, ignorePort) === this.getOrigin(url, ignorePort);
  }

  static isValid(url?: string) {
    if (typeof url !== 'string') {
      return false;
    }

    try {
      new URL(url);
    } catch (error) {
      return false;
    }

    return true;
  }
}

export {
  URLHelper
};
