<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;

/**
 * A UserJWT represents a JWT that has been issued to $this->getUser().
 * A JWT and a UserJWT are tied together through the uniqueId key (JWT) matching the $uniqueId property (UserJWT).
 *
 * If the User logs out, the matching UserJWT must be removed from database.
 *
 * On each authenticated request:
 *  - the uniqueID key from the JWT must have a matching UserJWT in database
 *  - that UserJWT must not be expired
 * If those two prerequisites are not met, the application must log out the User.
 * Furthermore, if a matching UserJWT is found but is expired, it must be removed from database.
 *
 * Expired UserJWTs must be removed from database periodically.
 *
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UserJWTRepository")
 */
class UserJWT
{
    /**
     * @var int|null
     * 
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="JWTs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var DateTime|null
     * 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $creationIp;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $creationUserAgent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=86, unique=true)
     */
    private $uniqueId;

    public function __construct(User $user, Request $request, string $uniqueId)
    {
        $this->user = $user;
        $this->createdAt = null;
        $this->expirationAt = null;
        $this->creationIp = $request->getClientIp();
        $this->creationUserAgent = $request->headers->get('user-agent');
        $this->uniqueId = $uniqueId;
    }

    public function isExpired(): bool
    {
        if (is_null($this->getExpirationAt())) {
            return true;
        }

        return $this->getExpirationAt()->getTimestamp() <= time();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getExpirationAt(): ?DateTime
    {
        return $this->expirationAt;
    }

    public function setExpirationAt(DateTime $expirationAt): self
    {
        $this->expirationAt = $expirationAt;

        return $this;
    }

    public function getCreationIp(): string
    {
        return $this->creationIp;
    }

    public function setCreationIp(string $creationIp): self
    {
        $this->creationIp = $creationIp;

        return $this;
    }

    public function getCreationUserAgent(): string
    {
        return $this->creationUserAgent;
    }

    public function setCreationUserAgent(string $creationUserAgent): self
    {
        $this->creationUserAgent = $creationUserAgent;

        return $this;
    }

    public function getUniqueId(): string
    {
        return $this->uniqueId;
    }

    public function setUniqueId(string $uniqueId): self
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }
}
