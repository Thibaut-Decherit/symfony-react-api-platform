import PropTypes from 'prop-types';
import React, {useCallback, useMemo} from 'react';
import Pagination from 'react-bootstrap/Pagination';
import {PaginatorItem} from './PaginatorItem';

const Paginator = (
  {
    itemsPerPage,
    totalItemsCount,
    currentPageNumber,
    setCurrentPageNumber
  }: {
    itemsPerPage: number;
    totalItemsCount: number;
    currentPageNumber: number;
    setCurrentPageNumber: (pageNumber: number) => void;
  }
) => {
  const calculatePagesCount = useCallback(() => {
    return Math.ceil(totalItemsCount / itemsPerPage);
  }, [itemsPerPage, totalItemsCount]);

  const goToPage = useCallback((pageNumber: number) => {
    if (pageNumber === currentPageNumber) {
      return;
    }

    setCurrentPageNumber(pageNumber);
  }, [currentPageNumber, setCurrentPageNumber]);

  const goToPreviousPage = useCallback(() => {
    if (currentPageNumber > 1) {
      goToPage(currentPageNumber - 1);
    }
  }, [currentPageNumber, goToPage]);

  const goToNextPage = useCallback(() => {
    if (currentPageNumber < calculatePagesCount()) {
      goToPage(currentPageNumber + 1);
    }
  }, [calculatePagesCount, currentPageNumber, goToPage]);

  const generatePaginationItems = useCallback(() => {
    const pagesCount = calculatePagesCount();
    const items = [];
    for (let pageNumber = 1; pageNumber <= pagesCount; pageNumber++) {
      items.push(
        <PaginatorItem
          key={pageNumber}
          currentPageNumber={currentPageNumber}
          onClick={goToPage}
          pageNumber={pageNumber}
        />
      );
    }

    // Prevents empty page if last item on last page has been deleted.
    if (pagesCount < currentPageNumber) {
      goToPreviousPage();
    }

    return items;
  }, [calculatePagesCount, currentPageNumber, goToPage, goToPreviousPage]);

  const items = useMemo(
    () => generatePaginationItems(),
    [generatePaginationItems]
  );

  if (items.length > 1) {
    return (
      <Pagination className="d-flex justify-content-center" size="sm">
        <Pagination.Prev onClick={goToPreviousPage}/>
        {items}
        <Pagination.Next onClick={goToNextPage}/>
      </Pagination>
    );
  } else {
    return null;
  }
};

Paginator.propTypes = {
  itemsPerPage: PropTypes.number.isRequired,
  totalItemsCount: PropTypes.number.isRequired,
  currentPageNumber: PropTypes.number.isRequired,
  setCurrentPageNumber: PropTypes.func.isRequired
};

export {
  Paginator
};
