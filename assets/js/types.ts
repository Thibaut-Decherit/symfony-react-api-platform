import {ChangeEvent, ChangeEventHandler, FormEvent} from 'react';
import type {Variant} from 'react-bootstrap/types';

export type ApiPlatformConstraintViolation = {
  code: string;
  message: string;
  propertyPath: string;
};

export type Credentials = {
  username: string;
  password: string;
};

export type FormErrors = {
  [key: string]: string[]
};

export type GlobalContext = {
  resetData: () => void;
  routeSsrProps: { [key: string]: any };
  ssrRoutes: SsrRoutes;
  stateUserInfo: UserInfo | Record<string, never>;
  setStateUserInfo: (data: UserInfo) => void;
} | Record<string, never>;

export type GlobalFormAlert = {
  message: string,
  variant: Variant
} | Record<string, never>;

export type ReactBootstrapFormControlChangeEvent = ChangeEvent<HTMLInputElement>;
export type ReactBootstrapFormControlChangeEventHandler = ChangeEventHandler<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>;

export type ReactBootstrapFormSubmitEventHandler = FormEvent;

export type SsrRoutes = {
  react_app_customer_edit_page: string;
  react_app_customer_index_page: string;
  react_app_customer_new_page: string;
  react_app_home_page: string;
  react_app_invoice_index_page: string;
  react_app_login_page: string;
};

export type UserInfo = {
  email: string;
  firstName: string;
  lastName: string;
  roles: string[];
};
