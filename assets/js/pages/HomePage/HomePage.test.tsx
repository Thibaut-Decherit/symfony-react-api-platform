import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {HomePage} from './HomePage';

describe('HomePage', () => {
  test('basic rendering', () => {
    render(<HomePage/>);
    expect(screen.getByRole('heading', {level: 1})).toBeVisible();
  });
});
