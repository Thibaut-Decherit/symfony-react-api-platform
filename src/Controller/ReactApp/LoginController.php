<?php

namespace App\Controller\ReactApp;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractReactAppController
{
    /**
     * @param Request $request
     * @Route(path="/login", name="react_app_login_page")
     * @return Response
     */
    public function loginPage(Request $request): Response
    {
        $routeSpecificProps = [];
        if (!is_null($request->query->get('fromUrlRelative'))) {
            $routeSpecificProps = [
                'LoginPage' => [
                    'LoginForm' => [
                        'fromUrlRelative' => urldecode($request->query->get('fromUrlRelative'))
                    ]
                ]
            ];
        }

        return $this->render('app.html.twig', [
            'props' => $this->buildProps(
                $request,
                $routeSpecificProps
            ),
            'title' => $this->buildDocumentTitle('Login')
        ]);
    }
}
