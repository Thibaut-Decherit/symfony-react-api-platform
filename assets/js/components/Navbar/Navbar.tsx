import {LogoutButton} from '@/components/LogoutButton';
import {GlobalContext} from '@/GlobalContextProvider';
import _ from 'lodash';
import React, {useContext} from 'react';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import {default as BootstrapNavbar} from 'react-bootstrap/Navbar';
import {Link, NavLink} from 'react-router-dom';

const Navbar = () => {
  const globalContext = useContext(GlobalContext);
  const isAuthenticated = !_.isEmpty(globalContext.stateUserInfo);

  return (
    <BootstrapNavbar bg="light" expand="lg">
      <NavLink to={globalContext.ssrRoutes.react_app_home_page}>
        <BootstrapNavbar.Brand>SymReact</BootstrapNavbar.Brand>
      </NavLink>
      <BootstrapNavbar.Toggle aria-controls="collapsible-nav"/>

      <BootstrapNavbar.Collapse id="collapsible-nav">
        <Nav className="mr-auto">
          {isAuthenticated && (
            <>
              <NavLink to={globalContext.ssrRoutes.react_app_customer_index_page} className={'nav-link'}>
                Customers
              </NavLink>
              <NavLink to={globalContext.ssrRoutes.react_app_invoice_index_page} className={'nav-link'}>
                Invoices
              </NavLink>
            </>
          )}
        </Nav>
        <div className="ml-auto d-flex">
          {!isAuthenticated
            ? (
              <>
                <Button className="mr-2">
                  Register
                </Button>
                <Link to={globalContext.ssrRoutes.react_app_login_page} state={{skipFrom: true}}>
                  <Button>
                    Login
                  </Button>
                </Link>
              </>
            )
            : (
              <>
                <div className="d-flex align-items-center mr-3">
                  <span>{
                    globalContext.stateUserInfo.firstName
                    + ' '
                    + globalContext.stateUserInfo.lastName
                  }</span>
                </div>
                <LogoutButton variant="secondary"/>
              </>
            )
          }
        </div>
      </BootstrapNavbar.Collapse>
    </BootstrapNavbar>
  );
};

export {
  Navbar
};
