<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

class JWTNotFoundListener
{
    private RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function onJWTNotFound(JWTNotFoundEvent $event): void
    {
        /*
         * Request has been done via Axios, so we want the response interceptor to detect the 401 response and don't
         * want to replace it with a 302 redirect.
         */
        if ($event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        $routeParams = [];
        if ($event->getRequest()->getRequestUri() !== '/') {
            $routeParams = [
                'fromUrlRelative' => urlencode($event->getRequest()->getRequestUri())
            ];
        }

        // Redirects to login page.
        $event->setResponse(
            new RedirectResponse(
                $this->router->generate('react_app_login_page', $routeParams)
            )
        );
    }
}
