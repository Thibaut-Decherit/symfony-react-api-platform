import type {GlobalContext, SsrRoutes, UserInfo} from '@/types';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {createContext, ReactNode, useCallback, useMemo, useReducer} from 'react';

const GlobalContext = createContext<GlobalContext>({});

const defaultData: { userInfo: Record<string, never> } = {
  userInfo: {}
};
const reducerUserInfo = (
  state: object,
  action: {
    data?: UserInfo;
    type: string;
  }
): UserInfo | Record<string, never> => {
  switch (action.type) {
    case 'reset':
    case 'update': {
      let newData;
      if (action.type === 'reset') {
        newData = _.cloneDeep(defaultData.userInfo);
      } else if (_.has(action, 'data') && action.data !== undefined) {
        newData = _.cloneDeep(action.data);
      } else {
        throw Error('action.data must be defined.');
      }

      return newData;
    }
  }
  throw Error('Unknown action.');
};

const GlobalContextProvider = (
  {
    children,
    routeSsrProps,
    ssrRoutes,
    userInfo
  }: {
    children: ReactNode;
    routeSsrProps: object;
    ssrRoutes: SsrRoutes;
    userInfo: UserInfo | null;
  }
) => {
  const [stateUserInfo, dispatchStateUserInfo] = useReducer(
    reducerUserInfo,
    userInfo !== null ? _.cloneDeep(userInfo) : {...defaultData.userInfo}
  );
  const setStateUserInfo = useCallback((data: UserInfo): void => {
    dispatchStateUserInfo({type: 'update', data});
  }, []);

  const resetData = useCallback(() => {
    const resettableDispatchers = [
      dispatchStateUserInfo
    ];

    resettableDispatchers.forEach(dispatch => {
      dispatch({type: 'reset'});
    });
  }, []);

  return (
    <GlobalContext.Provider
      value={useMemo((): GlobalContext => {
        return {
          resetData,
          routeSsrProps,
          ssrRoutes,
          stateUserInfo,
          setStateUserInfo
        }
      }, [resetData, routeSsrProps, ssrRoutes, stateUserInfo, setStateUserInfo])}
    >
      {children}
    </GlobalContext.Provider>
  );
};

GlobalContextProvider.propTypes = {
  children: PropTypes.object.isRequired,
  routeSsrProps: PropTypes.object,
  ssrRoutes: PropTypes.shape({
    react_app_customer_edit_page: PropTypes.string.isRequired,
    react_app_customer_index_page: PropTypes.string.isRequired,
    react_app_customer_new_page: PropTypes.string.isRequired,
    react_app_home_page: PropTypes.string.isRequired,
    react_app_invoice_index_page: PropTypes.string.isRequired,
    react_app_login_page: PropTypes.string.isRequired
  }).isRequired,
  userInfo: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    roles: PropTypes.arrayOf(PropTypes.string).isRequired
  })
};

export {
  GlobalContext,
  GlobalContextProvider
};
