import type {CustomerListCustomer} from '@/pages/CustomerIndexPage/CustomersList/CustomersList.types';
import type {CustomerPageCustomer} from '@/pages/CustomerPage/CustomerPage.types';
import {AxiosResponseWithCustomerPageCustomer} from '@/services/APIService/CustomerAPIService.interfaces';
import axios from 'axios';

const rootUrl = '/api/customers';

function add(customer: CustomerPageCustomer) {
  return new Promise<AxiosResponseWithCustomerPageCustomer>((resolve, reject) => {
    axios
      .post(
        rootUrl,
        customer,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(response => {
        if (response.status === 201) {
          resolve(response);
        } else {
          reject();
        }
      })
      .catch(error => {
        reject(error);
      });
  });
}

function deleteOneById(id: string) {
  return new Promise<void>((resolve, reject) => {
    axios
      .delete(rootUrl + '/' + id)
      .then(response => {
        if (response.status < 300) {
          resolve();
        } else {
          reject();
        }
      })
      .catch(error => {
        if (error.response?.status === 404) {
          resolve();
        } else {
          reject(error);
        }
      });
  });
}

function editOneById(id: string, customer: CustomerPageCustomer) {
  return new Promise<AxiosResponseWithCustomerPageCustomer>((resolve, reject) => {
    axios
      .patch(
        rootUrl + '/' + id,
        customer,
        {
          headers: {
            'Content-Type': 'application/merge-patch+json'
          }
        })
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error);
      });
  });
}

function findOneById(id: string) {
  return new Promise<CustomerPageCustomer>((resolve, reject) => {
    axios
      .get(rootUrl + '/' + id)
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
}

function paginatedFindByNameOrCompanyStartsBy(
  itemsPerPage = 5,
  pageNumber = 1,
  searchValue = ''
) {
  const url =
    rootUrl +
    '?pagination=true' +
    `&itemsPerPage=${itemsPerPage}` +
    `&page=${pageNumber}` +
    `&nameOrCompanyStartsBy=${searchValue}`;

  return new Promise<{
    results: CustomerListCustomer[];
    totalItemsCount: number
  }>((resolve, reject) => {
    axios
      .get(url)
      .then(response => {
        resolve({
          results: response.data['hydra:member'],
          totalItemsCount: response.data['hydra:totalItems']
        });
      })
      .catch(error => {
        reject(error);
      });
  });
}

export {
  add,
  deleteOneById,
  editOneById,
  findOneById,
  paginatedFindByNameOrCompanyStartsBy
};
