import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {LoginForm} from './LoginForm';

describe('LoginForm', () => {
  test('basic rendering', () => {
    render(<LoginForm/>);
    expect(screen.getByRole('form')).toBeVisible();
  });
});
