<?php

namespace App\Command;

use App\Entity\UserJWT;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RemoveExpiredUserJWTCommand
 * @package App\Command
 */
class RemoveExpiredUserJWTCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * RemoveExpiredUserJWTCommand constructor
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:remove-expired-user-jwts')
            ->setDescription('Removes expired UserJWTs.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Removing expired UserJTWs...');

        $this->endOutput($output, $this->em->getRepository(UserJWT::class)->removeExpired());
    }

    private function endOutput(OutputInterface $output, int $removedCount): void
    {
        $message = "<bg=green;fg=black>[OK] $removedCount UserJWT has been removed</>";

        if ($removedCount > 1) {
            $message = "<bg=green;fg=black>[OK] $removedCount UserJWTs have been removed</>";
        }

        $output->writeln($message);
    }
}
