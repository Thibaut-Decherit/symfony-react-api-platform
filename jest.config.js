/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/assets/js/$1"
    },
    preset: 'ts-jest',
    setupFilesAfterEnv: [
        '@testing-library/jest-dom/extend-expect',
        './assets/js/testing/mswServerSetup.ts',
        './assets/js/testing/reactTestingLibraryOverrides.tsx'
    ],
    testEnvironment: 'jest-environment-jsdom'
};
