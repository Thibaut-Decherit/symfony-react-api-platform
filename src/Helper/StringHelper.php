<?php

namespace App\Helper;

/**
 * Class StringHelper
 * Utility class for string formatting and manipulation.
 *
 * @package App\Helper
 */
class StringHelper
{
    const DEFAULT_ENCODING = 'UTF-8';

    /**
     * Supports extended charsets, lower and upper camel case.
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function camelToKebab(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return self::snakeToKebab(self::camelToSnake($string, $encoding));
    }

    /**
     * Supports extended charsets, lower and upper camel case.
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function camelToSnake(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        $result = '';
        $length = self::strLen($string, $encoding);
        $stringAsArray = mb_str_split($string, 1, $encoding);

        for ($i = 0; $i < $length; $i++) {
            if (self::isUppercase($stringAsArray[$i])) {
                // Adds underscore and converts current character to lowercase.
                $result .= '_' . self::strToLower($stringAsArray[$i], $encoding);
            } else {
                $result .= $stringAsArray[$i];
            }
        }

        // ltrim() will remove the starting _ added if $string was upper camel case.
        return ltrim($result, '_');
    }

    /**
     * Returns true if $string ends with $query, otherwise it returns false.
     * Supports extended charsets.
     *
     * @param string $string
     * @param string $query
     * @param string|null $encoding
     * @return bool
     */
    public static function endsWith(string $string, string $query, ?string $encoding = self::DEFAULT_ENCODING): bool
    {
        return mb_substr($string, -(self::strlen($query, $encoding)), null, $encoding) === $query;
    }

    /**
     * Removes:
     *  - whitespaces between HTML tags ('spaceless' Twig filter)
     *  - whitespaces after opening HTML tags and before closing HTML tags (removes whitespaces from the start and the
     *  end of text and icons, which are added on Twig rendering in place of Twig code)
     *
     * Both of these tend to render visually and mess with the spacing of elements, hence that function.
     *
     * @param string|null $html
     * @return string
     */
    public static function htmlCleanup(?string $html = null): string
    {
        /*
         * 'spaceless' Twig filter. Removes whitespaces between tags. Does not remove whitespaces right after the
         * opening tag and right before the closing tag of the same element.
         */
        $html = trim(preg_replace('/>\s+</', '><', $html ?? ''));

        // Removes single line and multiline comments.
        $html = preg_replace('/<!--(?:.|\s)*?-->/', '', $html);

        // Removes whitespaces right after the opening tag and right before the closing tag of the same element.
        $tags = implode(
            '|',
            [
                'a',
                'abbr',
                'b',
                'bdi',
                'bdo',
                'button',
                'cite',
                'code',
                'data',
                'dd',
                'del',
                'dfn',
                'div',
                'dt',
                'em',
                'figcaption',
                'h\d',
                'i',
                'ins',
                'kbd',
                'label',
                'li',
                'mark',
                'p',
                'q',
                's',
                'samp',
                'small',
                'span',
                'strong',
                'sub',
                'sup',
                'td',
                'th',
                'time',
                'tr',
                'u',
                'var'
            ]
        );
        return preg_replace_callback(
            '/(\s*<\s*\/?\s*[' . $tags . ']?[^>]*\/?\s*>\s*)/',
            function ($matches) {
                return trim($matches[1]);
            },
            $html
        );
    }

    /**
     * Supports extended charsets.
     *
     * @param string $string
     *
     * @param bool $fullString
     * If true, returns true only if the whole string is uppercase. If false, returns true if the string contains
     * uppercase characters.
     *
     * @return bool
     */
    public static function isUppercase(string $string, bool $fullString = true): bool
    {
        $pattern = '[\x{0041}-\x{005A}\x{00C0}-\x{00DE}\x{0141}\x{0152}\x{0160}\x{0178}\x{017D}\x{0391}-\x{03A9}]';

        if ($fullString) {
            $pattern = '^' . $pattern . '+$';
        }

        return preg_match("/$pattern/u", $string);
    }

    /**
     * Supports extended charsets.
     *
     * @param string $string
     * @param bool $upperCamel
     * @param string|null $encoding
     * @return string
     */
    public static function kebabToCamel(
        string $string,
        bool $upperCamel = false,
        ?string $encoding = self::DEFAULT_ENCODING
    ): string
    {
        return self::snakeToCamel(self::kebabToSnake($string), $upperCamel, $encoding);
    }

    /**
     * Supports extended charsets.
     *
     * @param string $string
     * @return string
     */
    public static function kebabToSnake(string $string): string
    {
        return str_replace('-', '_', $string);
    }

    /**
     * Removes accents from all characters in $string.
     *
     * @param string $string
     * @return string
     */
    public static function removeAccents(string $string): string
    {
        return transliterator_transliterate('Any-Latin; Latin-ASCII', $string);
    }

    /**
     * Removes $prefix from $string if it's at the beginning.
     * Else returns $string without modifying it.
     *
     * @param string $string
     * @param string $prefix
     * @param string|null $encoding
     * @return string
     */
    public static function removePrefix(
        string $string,
        string $prefix,
        ?string $encoding = self::DEFAULT_ENCODING
    ): string
    {
        if (self::startsWith($string, $prefix, $encoding)) {
            return mb_substr($string, self::strlen($prefix), null, $encoding);
        }

        return $string;
    }

    /**
     * Removes $suffix from $string if it's at the end.
     * Else returns $string without modifying it.
     *
     * @param string $string
     * @param string $suffix
     * @param string|null $encoding
     * @return string
     */
    public static function removeSuffix(
        string $string,
        string $suffix,
        ?string $encoding = self::DEFAULT_ENCODING
    ): string
    {
        if (self::endsWith($string, $suffix, $encoding)) {
            return mb_substr($string, 0, self::strlen($string) - self::strlen($suffix), $encoding);
        }

        return $string;
    }

    /**
     * Supports extended charsets.
     *
     * @param string $string
     * @param bool $upperCamel
     * @param string|null $encoding
     * @return string
     */
    public static function snakeToCamel(
        string $string,
        bool $upperCamel = false,
        ?string $encoding = self::DEFAULT_ENCODING
    ): string
    {
        $result = '';
        $length = self::strLen($string);
        $stringAsArray = mb_str_split($string, 1, $encoding);

        // Convert next character to uppercase or not.
        $up = false;

        for ($i = 0; $i < $length; $i++) {
            if ($stringAsArray[$i] === '_') {
                $up = true;
            } else {
                if ($up || ($upperCamel && $i === 0)) {
                    $result .= self::strToUpper($stringAsArray[$i]);
                    $up = false;
                } else {
                    $result .= $stringAsArray[$i];
                }
            }
        }

        return $result;
    }

    /**
     * Supports extended charsets.
     *
     * @param string $string
     * @return string
     */
    public static function snakeToKebab(string $string): string
    {
        return str_replace('_', '-', $string);
    }

    /**
     * Supports extended charsets.
     *
     * @param string $string
     * @param bool $upperCamel
     * @param string|null $encoding
     * @return string
     */
    public static function spaceToCamel(
        string $string,
        bool $upperCamel = false,
        ?string $encoding = self::DEFAULT_ENCODING
    ): string
    {
        return self::snakeToCamel(self::spaceToSnake($string, $encoding), $upperCamel, $encoding);
    }

    /**
     * Supports extended charsets.
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function spaceToKebab(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return self::snakeToKebab(self::spaceToSnake($string, $encoding));
    }

    /**
     * Supports extended charsets, converts string to lowercase.
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function spaceToSnake(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return str_replace(' ', '_', self::strToLower(trim($string), $encoding));
    }

    /**
     * Returns true if $string starts with $query, otherwise it returns false.
     * Supports extended charsets.
     *
     * @param string $string
     * @param string $query
     * @param string|null $encoding
     * @return bool
     */
    public static function startsWith(string $string, string $query, ?string $encoding = self::DEFAULT_ENCODING): bool
    {
        return mb_substr($string, 0, self::strlen($query, $encoding), $encoding) === $query;
    }

    /**
     * Supports extended charsets, unlike native strlen().
     *
     * @param string $string
     * @param string|null $encoding
     * @return int
     */
    public static function strLen(string $string, ?string $encoding = self::DEFAULT_ENCODING): int
    {
        return mb_strlen($string, $encoding);
    }

    /**
     * Supports extended charsets, unlike native str_split().
     *
     * @param string $string
     * @param int $length
     * @param string|null $encoding
     * @return string
     */
    public static function strSplit(string $string, int $length = 1, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return mb_str_split($string, $length, $encoding);
    }

    /**
     * Supports extended charsets, unlike native strtolower().
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function strToLower(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return mb_strtolower($string, $encoding);
    }

    /**
     * Supports extended charsets, unlike native strtoupper().
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function strToUpper(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return mb_strtoupper($string, $encoding);
    }

    /**
     * Prevents potential slowdown or DoS caused by feeding an extremely long string to a MySQL query.
     * Supports extended charsets.
     *
     * @param string $string
     * @param int $length
     * @param string|null $encoding
     * @return string
     */
    public static function truncateToMySQLVarcharMaxLength(
        string $string,
        int $length = 255,
        ?string $encoding = self::DEFAULT_ENCODING
    ): string
    {
        return mb_substr($string, 0, $length, $encoding);
    }

    /**
     * Prevents potential slowdown or DoS caused by hashing very long passwords.
     * Supports extended charsets.
     *
     * @param string $string
     * @param int $length
     * @param string|null $encoding
     * @return string
     */
    public static function truncateToPasswordHasherMaxLength(
        string $string,
        int $length = 4096,
        ?string $encoding = self::DEFAULT_ENCODING
    ): string
    {
        return mb_substr($string, 0, $length, $encoding);
    }

    /**
     * Supports extended charsets, unlike native ucfirst().
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function ucFirst(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return mb_strtoupper(mb_substr($string, 0, 1, $encoding), $encoding) . mb_substr($string, 1, null, $encoding);
    }

    /**
     * Supports extended charsets, unlike native ucwords().
     * Warning: Will convert to lowercase every character that is not the first character of a word, unlike native
     * ucwords().
     *
     * @param string $string
     * @param string|null $encoding
     * @return string
     */
    public static function ucWords(string $string, ?string $encoding = self::DEFAULT_ENCODING): string
    {
        return mb_convert_case($string, MB_CASE_TITLE, $encoding);
    }
}
