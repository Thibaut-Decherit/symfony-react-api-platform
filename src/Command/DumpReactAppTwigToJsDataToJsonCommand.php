<?php

namespace App\Command;

use App\Helper\StringHelper;
use App\Service\TwigToJsDataGeneratorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class DumpReactAppTwigToJsDataToJsonCommand extends Command
{
    private string $kernelProjectDir;
    private string $outputFilePath;
    private TwigToJsDataGeneratorService $twigToJsDataGeneratorService;
    private Filesystem $filesystem;

    public function __construct(
        string $kernelProjectDir,
        string $outputFilePath,
        TwigToJsDataGeneratorService $twigToJsDataGeneratorService,
        Filesystem $filesystem
    )
    {
        parent::__construct();

        $this->kernelProjectDir = $kernelProjectDir;
        $this->outputFilePath = $outputFilePath;
        $this->twigToJsDataGeneratorService = $twigToJsDataGeneratorService;
        $this->filesystem = $filesystem;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:generate-react-app-twig-to-js-data-json')
            ->setDescription(
                'Generates a JSON file to pass server side data to client side JS via a #twig-to-js-data DOM element'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Generating Twig to JS data JSON...');

        $this->filesystem->dumpFile(
            $this->outputFilePath,
            $this->twigToJsDataGeneratorService->generate()
        );

        $this->endOutput($output);
    }

    private function endOutput(OutputInterface $output): void
    {
        $relativeFilePath = StringHelper::removePrefix($this->outputFilePath, $this->kernelProjectDir . '/');
        $output->writeln("<bg=green;fg=black>[OK] React app Twig to JS data dumped to $relativeFilePath</>");
    }
}
