export type ToggleMaintainFocusOffCallback = () => void;
export type ToggleMaintainFocusOnCallback = () => void;
