import PropTypes from 'prop-types';
import React, {useMemo} from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import type {ButtonVariant} from 'react-bootstrap/types';
import {DropdownButtonItem} from './DropdownButtonItem';

const DropdownButton = (
  {
    block = false,
    callback = () => {
      console.error(
        'DropdownButton callback is not defined. You must pass a function that will be called when a choice is clicked and receives the clicked choice as parameter'
      );
    },
    choices,
    label = 'Button',
    size = 'sm',
    variant = 'primary'
  }: {
    block?: boolean;
    callback: (choice: string) => void;
    choices: string[];
    label?: string;
    size?: 'lg' | 'sm';
    variant?: ButtonVariant;
  }
) => {
  return (
    <Dropdown>
      <Dropdown.Toggle size={size} variant={variant} block={block}>{label}</Dropdown.Toggle>

      <Dropdown.Menu>
        {useMemo(() => {
          return choices.map(choice => {
            return (
              <DropdownButtonItem
                key={choice}
                choice={choice}
                onClick={callback}
              />
            );
          })
        }, [callback, choices])}
      </Dropdown.Menu>
    </Dropdown>
  );
};

DropdownButton.propTypes = {
  block: PropTypes.bool,
  callback: PropTypes.func.isRequired,
  choices: PropTypes.arrayOf(PropTypes.string).isRequired,
  label: PropTypes.string,
  size: PropTypes.string,
  variant: PropTypes.string
};

export {
  DropdownButton
};
