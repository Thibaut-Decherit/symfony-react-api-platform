<?php

namespace App\Controller;

use App\Entity\User;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AbstractDefaultController
 * @package App\Controller
 *
 * Centralize here data (e.g. constants) and logic (e.g. methods) that should be available to every controller.
 * Every controller must extend DefaultController.
 */
abstract class AbstractDefaultController extends AbstractController
{
    /**
     * AbstractDefaultController constructor
     *
     * Leave it even if it remains blank, so you don't have to modify the constructor of every single children of
     * DefaultController to add parent::__construct() the day DefaultController requires a constructor.
     */
    public function __construct()
    {

    }

    /**
     * Allows to get the right return type (User instead of UserInterface), enabling better IDE assistance and avoiding
     * polymorphic call error.
     *
     * @return User|null
     */
    protected function getUser(): ?User
    {
        $user = parent::getUser();

        if (!is_null($user) && !($user instanceof User)) {
            throw new LogicException(
                '$user should be an instance of App\Entity\User, instance of ' . get_class($user) . ' given'
            );
        }

        return $user;
    }
}
