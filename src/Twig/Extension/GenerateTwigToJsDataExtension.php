<?php

namespace App\Twig\Extension;

use App\Service\TwigToJsDataGeneratorService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GenerateTwigToJsDataExtension extends AbstractExtension
{
    private TwigToJsDataGeneratorService $generatorService;

    public function __construct(TwigToJsDataGeneratorService $generatorService)
    {
        $this->generatorService = $generatorService;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('generate_twig_to_js_data', [$this, 'generateTwigToJsData'])
        ];
    }

    public function generateTwigToJsData(): string
    {
        return $this->generatorService->generate();
    }
}
