<?php

namespace App\Helper;

/**
 * Class SanitizationHelper
 * Utility class for data sanitization.
 *
 * @package App\Helper
 */
class SanitizationHelper
{
    /**
     * Removes from $filename characters that are reserved by OSes or could allow directory traversal.
     * Based on characters banned in symfony/http-foundation/HeaderUtils.php::makeDisposition() and extended.
     *
     * @param string $filename
     * @return string
     */
    public static function filename(string $filename): string
    {
        return preg_replace('/[^\x20-\x7e\xa1-\xff]|[<>:"\/\\\|?*]/', '', $filename);
    }

    /**
     * @param string $string
     * @return bool
     */
    public static function hasDirectoryTraversal(string $string): bool
    {
        /*
         * Removes escape characters in case there are any, they may be there to attempt avoiding detection
         * (e.g. '..\/' or '\../').
         */
        $string = str_replace('\\', '', $string);

        return str_contains($string, '../');
    }

    /**
     * Escapes SQL wildcards to prevent wildcard DOS.
     * See https://www.owasp.org/index.php/Testing_for_SQL_Wildcard_Attacks_(OWASP-DS-001)
     *
     * @param string $string
     * @return string
     */
    public static function sqlWildcards(string $string): string
    {
        return addcslashes($string, '\%_[]^-');
    }
}
