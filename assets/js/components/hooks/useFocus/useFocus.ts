import {useCallback, useRef} from 'react';
import type {CaretPosition} from './useFocus.types';

const moveCaret = <T extends HTMLInputElement | HTMLTextAreaElement>(
  element: T,
  position: 'end' | 'selectAll' | 'start'
) => {
  let start;
  let end;

  switch (position) {
    case 'end':
      start = element.value.length;
      end = start;
      break;
    case 'selectAll':
      start = 0;
      end = element.value.length;
      break;
    case 'start':
      start = 0;
      end = 0;
      break;
  }

  element.setSelectionRange(start, end);
}

/**
 * @param {CaretPosition} caretPosition
 * Only used if the ref is assigned to an input or a textarea element.
 *
 * If 'current' and the user does not have interacted with the field yet, the caret will be positioned to the start.
 *
 * Warning: Set it to anything else than 'current' or 'endOnFirstCall' with caution, it may have a negative UX impact as
 * the user may not be expecting/wanting the caret to move.
 */
const useFocus = <T extends HTMLElement>(caretPosition: CaretPosition = 'current') => {
  const ref = useRef<T>(null);
  let setFocusCallCount = 0;

  const setFocus = useCallback(() => {
    setFocusCallCount++;

    const element = ref?.current;

    element?.focus?.();

    if (element instanceof HTMLInputElement || element instanceof HTMLTextAreaElement) {
      if (
        caretPosition === 'end'
        || (caretPosition === 'endOnFirstCall' && setFocusCallCount < 2)
      ) {
        moveCaret(element, 'end');
      } else if (caretPosition === 'selectAll') {
        moveCaret(element, 'selectAll');
      } else if (caretPosition === 'start') {
        moveCaret(element, 'start');
      }
    }
  }, [caretPosition, setFocusCallCount]);

  return [ref, setFocus] as const;
}

export {
  useFocus
};
