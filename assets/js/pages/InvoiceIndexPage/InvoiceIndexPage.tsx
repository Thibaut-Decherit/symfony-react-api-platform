import {setTitle} from '@/services/DocumentService';
import React from 'react';
import {InvoicesList} from './InvoicesList';

const InvoiceIndexPage = () => {
  setTitle('Invoices');

  return (
    <>
      <h1>Invoices</h1>
      <InvoicesList/>
    </>
  );
};

export {
  InvoiceIndexPage
};
