import {GlobalContext} from '@/GlobalContextProvider';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {ReactNode, useContext, useEffect} from 'react';
import {Navigate, useLocation} from 'react-router-dom';

/*
 * If Axios request fails because user lacks authentication (401), calls globalContext.resetData() and redirects to
 * login page.
 */
const AuthenticatedRoute = (
  {
    children
  }: {
    children: ReactNode
  }
) => {
  const globalContext = useContext(GlobalContext);
  const location = useLocation();

  useEffect(() => {
    const handleRequestFailedBecauseUnauthenticated = () => {
      globalContext.resetData();
    };

    window.addEventListener('request-failed-because-unauthenticated', handleRequestFailedBecauseUnauthenticated);

    return () => {
      window.removeEventListener(
        'request-failed-because-unauthenticated',
        handleRequestFailedBecauseUnauthenticated
      );
    };
  }, [globalContext]);

  return (
    <>
      {!_.isEmpty(globalContext.stateUserInfo)
        ? children
        : <Navigate to={globalContext.ssrRoutes.react_app_login_page} state={{from: location}} replace/>
      }
    </>
  );
};

AuthenticatedRoute.propTypes = {
  children: PropTypes.object.isRequired
};

export {
  AuthenticatedRoute
};
