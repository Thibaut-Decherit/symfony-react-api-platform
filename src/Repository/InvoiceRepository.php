<?php

namespace App\Repository;

use App\Entity\Invoice;
use App\Entity\User;
use App\Helper\SanitizationHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Invoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invoice[]    findAll()
 * @method Invoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Invoice::class);
        $this->em = $em;
    }

    /**
     * @param array $searchParams
     * @param User|null $owner
     * @return array
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function reactAppIndexSearch(array $searchParams, ?User $owner): array
    {
        $qb = $this->createQueryBuilder('i');

        $qb->join('i.customer', 'c');

        $qb
            ->andWhere('c.user = :owner')
            ->setParameter('owner', $owner);

        if (array_key_exists('search', $searchParams)) {
            $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->like('c.firstName', ':search'),
                        $qb->expr()->like('c.lastName', ':search'),
                        $qb->expr()->like('c.company', ':search')
                    )
                );
            $qb->setParameter(
                'search',
                SanitizationHelper::sqlWildcards($searchParams['search']) . '%'
            );
        }

        /**
         * Clone allows the QueryBuilder to be reused to make multiples queries without having to reconfigure it from
         * scratch.
         */
        $countQb = clone $qb;
        $totalCount = $countQb
            ->select('count(i.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $qb
            ->setMaxResults($searchParams['itemsPerPage'])
            ->setFirstResult(($searchParams['page'] - 1) * $searchParams['itemsPerPage']);


        return [
            'results' => $qb->getQuery()->getResult(),
            'totalItemsCount' => $totalCount
        ];
    }

    /**
     * Sets chrono of oldest Invoice with null chrono and owned by given User.
     * Chrono is set to highest chrono in Invoices owned by given User + 1.
     *
     * @param User $user
     * @return mixed
     * @throws DBALException
     */
    public function setIncrementedChrono(User $user)
    {
        $customerTable = 'customer';
        $invoiceTable = 'invoice';

        $query = "
                UPDATE $invoiceTable AS i
                SET i.chrono = (
                    SELECT chrono
                    FROM (SELECT chrono, customer_id FROM $invoiceTable) AS i2
                    WHERE i2.customer_id IN (
                        SELECT id
                        FROM (
                            SELECT id FROM $customerTable
                            WHERE user_id = :userId
                        ) AS c
                    )
                    ORDER BY i2.chrono DESC
                    LIMIT 1
                ) + 1
                WHERE i.id = (
                    SELECT id
                    FROM (SELECT id, chrono, customer_id FROM $invoiceTable) AS i3
                    WHERE i3.chrono IS NULL
                    AND i3.customer_id IN (
                        SELECT id
                        FROM (
                            SELECT id FROM $customerTable
                            WHERE user_id = :userId
                        ) AS c2
                    )
                    ORDER BY id ASC
                    LIMIT 1
                )
            ";

        return $this->em
            ->getConnection()->prepare($query)
            ->execute([
                'userId' => $user->getId()
            ]);
    }
}
