import {GlobalContext} from '@/GlobalContextProvider';
import {canUseWindow} from '@/helpers/SsrHelper';
import PropTypes from 'prop-types';
import {useContext} from 'react';
import {To, useNavigate} from 'react-router-dom';

/**
 * @param {?string} defaultTo
 * Will be used if history does not contain any entry.
 * If undefined, defaults to globalContext.ssrRoutes.react_app_home_page.
 *
 * @param {boolean} replace
 * @return {function}
 */
const useNavigateBackward = (
  defaultTo?: To,
  replace = false
) => {
  const globalContext = useContext(GlobalContext);
  const navigate = useNavigate();

  return () => {
    if (canUseWindow() && window?.history?.state?.idx > 0) {
      navigate(-1);
    } else {
      if (defaultTo === undefined) {
        defaultTo = globalContext.ssrRoutes.react_app_home_page;
      }

      navigate(defaultTo, {replace});
    }
  }
}

useNavigateBackward.propTypes = {
  defaultTo: PropTypes.string,
  replace: PropTypes.bool
};

export {
  useNavigateBackward
};
