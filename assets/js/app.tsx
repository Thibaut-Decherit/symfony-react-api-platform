import {AuthenticatedRoute} from '@/components/AuthenticatedRoute';
import {Navbar} from '@/components/Navbar';
import {GlobalContextProvider} from '@/GlobalContextProvider';
import {isSsr} from '@/helpers/SsrHelper';
import '@/lib/axios';
import {CustomerIndexPage} from '@/pages/CustomerIndexPage';
import {CustomerPage} from '@/pages/CustomerPage';
import {HomePage} from '@/pages/HomePage';
import {InvoiceIndexPage} from '@/pages/InvoiceIndexPage';
import {LoginPage} from '@/pages/LoginPage';
import type {SsrRoutes, UserInfo} from '@/types';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import Container from 'react-bootstrap/Container';
import ReactOnRails from 'react-on-rails';
import {BrowserRouter, MemoryRouter, Route, Routes} from 'react-router-dom';

/*
Warning: Might be a placebo hiding the error more than a proper fix, or unsuitable in some cases. Keep an eye on
components improperly rendered server side.
 */
React.useLayoutEffect = isSsr() ? React.useEffect : React.useLayoutEffect;

const App = (
  {
    routeSsrProps,
    ssrRoute,
    ssrRoutes,
    userInfo
  }: {
    routeSsrProps: object;
    ssrRoute: string;
    ssrRoutes: SsrRoutes;
    userInfo: UserInfo | null;
  }
) => {
  // BrowserRouter should not be used server side because window variable is undefined.
  const Router = isSsr() ? MemoryRouter : BrowserRouter;

  return (
    <GlobalContextProvider
      routeSsrProps={_.cloneDeep(routeSsrProps)}
      ssrRoutes={{...ssrRoutes}}
      userInfo={_.cloneDeep(userInfo)}
    >
      <Router initialEntries={[ssrRoute]}>
        <Navbar/>
        <Container className="my-5">
          <Routes>
            <Route
              path={ssrRoutes.react_app_customer_index_page}
              element={
                <AuthenticatedRoute>
                  <CustomerIndexPage/>
                </AuthenticatedRoute>
              }
            />
            <Route
              // Path doubles as new page, where id parameter will be 'new'.
              path={ssrRoutes.react_app_customer_edit_page}
              element={
                <AuthenticatedRoute>
                  <CustomerPage/>
                </AuthenticatedRoute>
              }
            />
            <Route
              path={ssrRoutes.react_app_home_page}
              element={
                <AuthenticatedRoute>
                  <HomePage/>
                </AuthenticatedRoute>
              }
            />
            <Route
              path={ssrRoutes.react_app_invoice_index_page}
              element={
                <AuthenticatedRoute>
                  <InvoiceIndexPage/>
                </AuthenticatedRoute>
              }
            />
            <Route path={ssrRoutes.react_app_login_page} element={<LoginPage/>}/>
          </Routes>
        </Container>
      </Router>
    </GlobalContextProvider>
  );
};

App.propTypes = {
  routeSsrProps: PropTypes.object,
  ssrRoute: PropTypes.string,
  ssrRoutes: PropTypes.object,
  userInfo: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    roles: PropTypes.array.isRequired
  })
};

// @ts-ignore
ReactOnRails.register({App});
