<?php

namespace App\Doctrine\CurrentUserExtension\CurrentUserExtensionEntity;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class CurrentUserExtensionInvoice implements CurrentUserExtensionEntityInterface
{
    public function addWhereCurrentUser(
        QueryBuilder $queryBuilder,
        string $rootAlias,
        string $userParameterName,
        QueryNameGeneratorInterface $queryNameGenerator
    ): void
    {
        // Generates a unique alias name to avoid collisions with other extensions or filters.
        $customerAliasName = $queryNameGenerator->generateJoinAlias('customer');

        $queryBuilder
            ->join("$rootAlias.customer", $customerAliasName)
            ->andWhere("$customerAliasName.user = :$userParameterName");
    }
}
