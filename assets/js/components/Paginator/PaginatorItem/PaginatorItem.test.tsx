import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {PaginatorItem} from './PaginatorItem';

describe('PaginatorItem', () => {
  test('basic rendering', () => {
    render(<PaginatorItem currentPageNumber={1} onClick={jest.fn()} pageNumber={1}/>);
    expect(screen.getByRole('listitem')).toBeVisible();
  });
});
