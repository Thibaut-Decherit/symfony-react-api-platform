import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {DropdownButtonItem} from './DropdownButtonItem';

describe('DropdownButtonItem', () => {
  test('basic rendering', () => {
    render(<DropdownButtonItem choice={'test'} onClick={jest.fn()}/>);
    expect(screen.getByRole('button')).toBeVisible();
  });
});
