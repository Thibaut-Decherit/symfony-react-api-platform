export type CaretPosition = 'current' | 'end' | 'endOnFirstCall' | 'selectAll' | 'start';
