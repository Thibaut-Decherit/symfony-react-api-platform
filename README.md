# Symfony 4 React API Platform
Application coded during [this course](https://learn.web-develop.me/courses/developpement-moderne-avec-apiplatform-symfony-et-react).\
I deviated from the course quite a lot to add more features and explore server side rendering with
[Limenius/ReactBundle](https://github.com/Limenius/ReactBundle).

## Setup
### Install dependencies
`composer install`
`yarn`

### Create `.env.local` (see `.env.your_env.local.dist`)
Customize all empty variables.

If you use MariaDB, `DB_VERSION` should start with `mariadb-` then the version number, else only specifying the version
number should suffice.

### Setup LexikJWTAuthenticationBundle
`php bin/console lexik:jwt:generate-keypair`

### Setup database
`php bin/console doctrine:database:create`
`php bin/console doctrine:migration:migrate`

## Run in dev env
Run in dedicated terminals:
- `symfony server:start`
- `yarn encore:dev`
- `yarn encore:dev:serverside`
- `node renderingExternalServer.js`

## Deployment
You may need to add `export PATH=$PATH:/home/{user}/.nvm/versions/node/{version}/bin` (replace `{user}` and
`{version}`) to `/etc/apache2/envvars`, so the [SSR bundle](https://github.com/Limenius/ReactBundle) is able to run JS
server side.

`pm2 start renderingExternalServer.js --name symfony-react-api-platform-rendering`
`pm2 save`
