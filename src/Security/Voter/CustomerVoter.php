<?php

namespace App\Security\Voter;

use App\Entity\Customer;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CustomerVoter extends Voter
{
    const ATTRIBUTE_CUSTOMER_DELETE = 'CUSTOMER_DELETE';
    const SUPPORTED_ATTRIBUTES = [
        self::ATTRIBUTE_CUSTOMER_DELETE
    ];
    const SUPPORTED_SUBJECTS = [
        Customer::class
    ];

    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, self::SUPPORTED_ATTRIBUTES)) {
            return false;
        }

        if (!is_object($subject) || !in_array(get_class($subject), self::SUPPORTED_SUBJECTS)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param Customer $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::ATTRIBUTE_CUSTOMER_DELETE:
                if ($subject->getUser() === $token->getUser() && $subject->getInvoices()->isEmpty()) {
                    return true;
                }
                break;
            default:
                throw new LogicException('$attribute ' . $attribute . ' is not supported.');
        }

        return false;
    }
}
