<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\User;
use App\Helper\SanitizationHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    /**
     * @param array $searchParams
     * @param User|null $owner
     * @return array
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function reactAppIndexSearch(array $searchParams, ?User $owner): array
    {
        $qb = $this->createQueryBuilder('c');

        $qb
            ->andWhere('c.user = :owner')
            ->setParameter('owner', $owner);

        if (array_key_exists('search', $searchParams)) {
            $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->like('c.firstName', ':search'),
                        $qb->expr()->like('c.lastName', ':search'),
                        $qb->expr()->like('c.company', ':search')
                    )
                );
            $qb->setParameter(
                'search',
                SanitizationHelper::sqlWildcards($searchParams['search']) . '%'
            );
        }

        /**
         * Clone allows the QueryBuilder to be reused to make multiples queries without having to reconfigure it from
         * scratch.
         */
        $countQb = clone $qb;
        $totalCount = $countQb
            ->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $qb
            ->setMaxResults($searchParams['itemsPerPage'])
            ->setFirstResult(($searchParams['page'] - 1) * $searchParams['itemsPerPage']);


        return [
            'results' => $qb->getQuery()->getResult(),
            'totalItemsCount' => $totalCount
        ];
    }
}
