import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {SearchBar} from './SearchBar';

describe('SearchBar', () => {
  test('basic rendering', () => {
    render(<SearchBar onSubmit={jest.fn()}/>);
    expect(screen.getByRole('textbox')).toBeVisible();
    expect(screen.getByRole('button')).toBeVisible();
  });
});
