import {URLHelper} from '@/helpers/URLHelper';
import axios from 'axios';

/*
The X-Requested-With header is required for $request->isXmlHttpRequest() in a Symfony controller.
Add to this array any other origin where that header might be needed.
 */
const XRequestedWithSupportedOrigins = [
  /*
  Port is removed because new URL(url).origin seems to inconsistently include it or not, which messes with string
  comparisons.
   */
  URLHelper.getOrigin(undefined, true)
];

axios.interceptors.request.use(config => {
  /*
  Adds X-Requested-With header if possible. Some APIs may not support it according to the
  Access-Control-Allow-Headers header from their CORS preflight response.
  Port is removed because new URL(url).origin seems to inconsistently include it or not, which messes with origin
  matching.
   */
  if (
    URLHelper.isValid(config?.url)
    && XRequestedWithSupportedOrigins.includes(URLHelper.getOrigin(config?.url, true))
    && config?.headers !== undefined
  ) {
    config.headers['X-Requested-With'] = 'XMLHttpRequest';
  }

  return config;
}, error => {
  return Promise.reject(error);
});

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    /*
    Port is removed because new URL(url).origin seems to inconsistently include it or not, which messes with string
    comparisons.
     */
    if (
      URLHelper.isSameOrigin(error.response?.request?.responseURL, true)
      && error.response?.status === 401
      && error.response?.data?.message !== 'Bad credentials.'
    ) {
      // Caught by AuthenticatedRoute if the user is on an authenticated only page.
      window.dispatchEvent(new Event('request-failed-because-unauthenticated'));
    }

    return Promise.reject(error);
  }
);
