import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {FormGroup} from './FormGroup';

describe('FormGroup', () => {
  test('basic rendering', () => {
    render(<FormGroup label="test label" name="test" onChange={jest.fn()} prefix="test"/>);
    expect(screen.getByText('test label')).toBeVisible();
    expect(screen.getByRole('textbox')).toBeVisible();
  });
});
