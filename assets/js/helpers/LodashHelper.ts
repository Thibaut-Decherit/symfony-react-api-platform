import _ from 'lodash';

class LodashHelper {
  static hasAll(object: object, paths: string[]) {
    return paths.every(value => {
      return _.has(object, value);
    });
  }
}

export {
  LodashHelper
};
