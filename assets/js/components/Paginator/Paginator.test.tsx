import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {Paginator} from './Paginator';

describe('Paginator', () => {
  test('basic rendering', () => {
    render(<Paginator itemsPerPage={5} totalItemsCount={10} currentPageNumber={1} setCurrentPageNumber={jest.fn()}/>);
    expect(screen.getByRole('list')).toBeVisible();
  });
});
