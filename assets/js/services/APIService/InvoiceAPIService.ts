import type {InvoiceListInvoice} from '@/pages/InvoiceIndexPage/InvoicesList/InvoicesList.types';
import axios from 'axios';

function deleteOneById(id: string) {
  return new Promise<void>((resolve, reject) => {
    axios
      .delete('/api/invoices/' + id)
      .then(response => {
        if (response.status < 300) {
          resolve();
        } else {
          reject();
        }
      })
      .catch(error => {
        console.error(error);

        reject();
      });
  });
}

function paginatedFindByNameOrCompanyStartsBy(
  itemsPerPage = 5,
  pageNumber = 1,
  searchValue = ''
) {
  const url =
    '/api/invoices' +
    '?pagination=true' +
    `&itemsPerPage=${itemsPerPage}` +
    `&page=${pageNumber}` +
    `&nameOrCompanyStartsBy=${searchValue}`;

  return new Promise<{
    results: InvoiceListInvoice[];
    totalItemsCount: number
  }>((resolve, reject) => {
    axios
      .get(url)
      .then(response => {
        resolve({
          results: response.data['hydra:member'],
          totalItemsCount: response.data['hydra:totalItems']
        });
      })
      .catch(error => {
        console.error(error);

        reject(error);
      });
  });
}

export {
  deleteOneById,
  paginatedFindByNameOrCompanyStartsBy
};
