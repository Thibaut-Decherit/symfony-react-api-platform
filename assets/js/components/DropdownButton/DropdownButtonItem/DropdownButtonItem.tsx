import PropTypes from 'prop-types';
import React, {useCallback} from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

const DropdownButtonItem = (
  {
    choice,
    onClick
  }: {
    choice: string;
    onClick: (choice: string) => void;
  }
) => {
  const handleClick = useCallback(() => {
    onClick(choice);
  }, [choice, onClick]);

  return (
    <Dropdown.Item key={choice} onClick={handleClick}>{choice}</Dropdown.Item>
  );
};

DropdownButtonItem.propTypes = {
  choice: PropTypes.oneOfType([
    PropTypes.string
  ]).isRequired,
  onClick: PropTypes.func.isRequired
};

export {
  DropdownButtonItem
};
