<?php

namespace App\Singleton;

use Exception;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Parser\DecimalMoneyParser;

/**
 * Class MoneySingleton
 *
 * Provides access to specific Money classes as singleton instances to avoid redundant instantiations.
 *
 * @package App\Singleton
 */
class MoneySingleton
{
    private static MoneySingleton $instance;

    /**
     * @var Currency[]
     */
    private array $currencies;

    private DecimalMoneyParser $decimalMoneyParser;
    private DecimalMoneyFormatter $decimalMoneyFormatter;
    private ISOCurrencies $ISOCurrencies;

    /**
     * The Singleton's constructor should always be private to prevent direct construction calls with the `new`
     * operator.
     */
    private function __construct()
    {
    }

    /**
     * Singletons should not be restorable from strings.
     *
     * @return never
     * @throws Exception
     */
    public function __wakeup(): never
    {
        throw new Exception('Cannot unserialize a singleton.');
    }


    /**
     * This is the static method that controls the access to the singleton instance.
     * On the first run, it creates a singleton instance and places it into the static field.
     * On subsequent runs, it returns the existing instance stored in the static field.
     */
    public static function getInstance(): static
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function getCurrency(string $code): Currency
    {
        if (!isset($this->currencies[$code])) {
            $this->currencies[$code] = new Currency($code);
        }

        return $this->currencies[$code];
    }

    public function getDecimalMoneyFormatter(): DecimalMoneyFormatter
    {
        if (!isset($this->decimalMoneyFormatter)) {
            $this->decimalMoneyFormatter = new DecimalMoneyFormatter($this->getISOCurrencies());
        }

        return $this->decimalMoneyFormatter;
    }

    public function getDecimalMoneyParser(): DecimalMoneyParser
    {
        if (!isset($this->decimalMoneyParser)) {
            $this->decimalMoneyParser = new DecimalMoneyParser($this->getISOCurrencies());
        }

        return $this->decimalMoneyParser;
    }

    private function getISOCurrencies(): ISOCurrencies
    {
        if (!isset($this->ISOCurrencies)) {
            $this->ISOCurrencies = new ISOCurrencies();
        }

        return $this->ISOCurrencies;
    }
}
