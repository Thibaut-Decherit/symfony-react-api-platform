import {LockableButton} from '@/components/LockableButton';
import {GlobalContext} from '@/GlobalContextProvider';
import {logout} from '@/services/AuthenticationService';
import PropTypes from 'prop-types';
import React, {useCallback, useContext, useState} from 'react';
import type {ButtonVariant} from 'react-bootstrap/types';
import {useNavigate} from 'react-router-dom';

const LogoutButton = (
  {
    variant = 'primary'
  }: {
    variant?: ButtonVariant;
  }
) => {
  const globalContext = useContext(GlobalContext);
  const [stateDisabled, setStateDisabled] = useState(false);
  const navigate = useNavigate();

  const handleLogout = useCallback(() => {
    setStateDisabled(true);

    logout(() => {
      globalContext.resetData();
    })
      .then(() => {
        navigate(globalContext.ssrRoutes.react_app_login_page)
      });
  }, [globalContext, navigate]);

  return (
    <LockableButton disabled={stateDisabled} onClick={handleLogout} variant={variant}>
      <span aria-hidden={true} className="fa fa-sign-out mr-2" data-lockable-button-icon=""/>
      Logout
    </LockableButton>
  );
};

LogoutButton.propTypes = {
  variant: PropTypes.string
};

export {
  LogoutButton
};
