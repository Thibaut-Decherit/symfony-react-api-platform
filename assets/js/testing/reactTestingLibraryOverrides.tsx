import {GlobalContextProvider} from '@/GlobalContextProvider';
import * as ssrRoutes from '@/testing/routes.json';
import * as twigToJsData from '@/testing/twigToJsData.json';
import type {UserInfo} from '@/types';
import {render, RenderOptions} from '@testing-library/react';
import React, {ReactElement} from 'react';
import {MemoryRouter} from 'react-router-dom';

type CustomRenderOptions = {
  route: string,
  userInfo?: UserInfo | null
};

const customRenderDefaultOptions: CustomRenderOptions = {
  route: '/',
  userInfo: null
};
const customRender = (
  ui: ReactElement,
  options: CustomRenderOptions = customRenderDefaultOptions,
  renderOptions?: RenderOptions
) => {
  // Overwrites customRenderDefaultOptions properties declared in options.
  options = {...customRenderDefaultOptions, ...options};

  const uiWithWrappers = (
    <>
      <GlobalContextProvider routeSsrProps={{}} ssrRoutes={ssrRoutes} userInfo={options.userInfo || null}>
        <MemoryRouter initialEntries={[options.route]}>
          {ui}
        </MemoryRouter>
      </GlobalContextProvider>
      <div id="twig-to-js-data" data-twig-to-js={JSON.stringify(twigToJsData)}/>
    </>
  );

  return render(uiWithWrappers, renderOptions);
};

export * from '@testing-library/react';
export {customRender as render};
