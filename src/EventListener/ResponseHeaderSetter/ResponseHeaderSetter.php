<?php

namespace App\EventListener\ResponseHeaderSetter;

use App\EventListener\ResponseHeaderSetter\DynamicResponseHeaderSetter\CspHeaderSetter;
use App\EventListener\ResponseHeaderSetter\DynamicResponseHeaderSetter\ResponseAuthenticityHeaderSetter;
use App\Service\SessionTokenService;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ResponseHeaderSetter
 *
 * Adds custom headers to every response. Dynamic headers are generated and set in their dedicated class within
 * App\EventListener\ResponseHeaderSetter\DynamicResponseHeaderSetter namespace.
 *
 * @package App\EventListener\ResponseHeaderSetter
 */
class ResponseHeaderSetter implements EventSubscriberInterface
{
    private string $kernelEnvironment;

    private array $simpleHeaders;

    private RequestStack $requestStack;

    private array $cspConfig;

    private SessionTokenService $sessionTokenService;

    private RouterInterface $router;

    private SessionInterface $session;

    /**
     * ResponseHeaderSetter constructor
     *
     * @param string $kernelEnvironment
     * @param array $simpleHeaders
     * @param RequestStack $requestStack
     * @param array $cspConfig
     * @param SessionTokenService $sessionTokenService
     * @param RouterInterface $router
     * @param SessionInterface $session
     */
    public function __construct(
        string $kernelEnvironment,
        array $simpleHeaders,
        RequestStack $requestStack,
        array $cspConfig,
        SessionTokenService $sessionTokenService,
        RouterInterface $router,
        SessionInterface $session
    )
    {
        $this->kernelEnvironment = $kernelEnvironment;
        $this->simpleHeaders = $simpleHeaders;
        $this->requestStack = $requestStack;
        $this->cspConfig = $cspConfig;
        $this->sessionTokenService = $sessionTokenService;
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * @param ResponseEvent $event
     * @throws Exception
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        if ($this->supports($event) === false) {
            return;
        }

        $this->setDynamicHeaders($event);
        $this->setStaticHeaders($event);
    }

    /**
     * @param ResponseEvent $event
     * @return bool
     */
    private function supports(ResponseEvent $event): bool
    {
        /*
         * Required to avoid wasting resources by triggering the listener on sub-requests (e.g. when embedding
         * controllers in templates).
         */
        if ($event->isMasterRequest() === false) {
            return false;
        }

        /*
         * Failsafe, in some rare instances $this->requestStack->getMasterRequest() might return null.
         */
        if (is_null($this->requestStack->getMasterRequest())) {
            return false;
        }

        return true;
    }

    /**
     * Sets headers requiring a dedicated class to generate them according to specific parameters (e.g. app environment,
     * requested route...).
     *
     * @param ResponseEvent $event
     * @throws Exception
     */
    private function setDynamicHeaders(ResponseEvent $event)
    {
        $responseHeaders = $event->getResponse()->headers;

        (new CspHeaderSetter(
            $this->kernelEnvironment,
            $this->requestStack,
            $responseHeaders,
            $this->cspConfig,
            $this->sessionTokenService,
            $this->router
        ))->set();

        (new ResponseAuthenticityHeaderSetter($responseHeaders, $this->sessionTokenService))->set();
    }

    /**
     * Sets headers specified in config.yml.
     *
     * @param ResponseEvent $event
     */
    private function setStaticHeaders(ResponseEvent $event)
    {
        $responseHeaders = $event->getResponse()->headers;
        foreach ($this->simpleHeaders as $headerName => $headerValue) {
            $responseHeaders->set($headerName, $headerValue);
        }
    }
    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::RESPONSE => 'onKernelResponse'];
    }
}
