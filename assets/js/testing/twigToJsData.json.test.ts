import * as twigToJsData from '@/testing/twigToJsData.json';
import _ from 'lodash';

describe('TwigDataService', () => {
  test('no missing translations', () => {
    const missingTranslations: string[] = [];

    const parser = (value: object | string, key: string, keyPrefix = '') => {
      const newKeyPrefix = ((keyPrefix !== '') ? (keyPrefix + '.') : keyPrefix) + key

      if (typeof value === 'object') {
        /*
        We did not reach the translation yet and are still iterating over the fragments of its key, so we need to go
        deeper by calling parser() recursively.
         */
        _.forEach(value, (value: object | string, key: string) => {
          parser(value, key, newKeyPrefix);
        });
      } else if (value === newKeyPrefix) {
        /*
        We reached the translation and it is indeed missing: Symfony translator replaces missing translations with their
        key.
         */
        missingTranslations.push(value);
      }
    };

    _.forEach(twigToJsData.translations, (value: object | string, key: string) => {
      parser(value, key);
    });

    expect(missingTranslations).toEqual([]);
  });
});
