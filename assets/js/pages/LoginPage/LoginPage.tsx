import {GlobalContext} from '@/GlobalContextProvider';
import {LoginForm} from '@/pages/LoginPage/LoginForm';
import {setTitle} from '@/services/DocumentService';
import _ from 'lodash';
import React, {useContext, useEffect, useMemo} from 'react';
import {Navigate, useLocation} from 'react-router-dom';

/*
Declared outside the component to preserve the value between the component's instances. Must be set to true in a
useEffect() after first client side render.
 */
let ignoreSsrProps = false;

const LoginPage = () => {
  setTitle('Login');

  const globalContext = useContext(GlobalContext);
  const ssrProps: { fromUrlRelative?: string } = globalContext.routeSsrProps?.LoginPage?.LoginForm;
  const location = useLocation();

  /*
  useMemo() to preserve the value between renders, else after first render the value will change in case of SSR
  because ignoreSsrProps will no longer be false.
   */
  const to: string = useMemo(() => {
    if (_.has(location, 'state.from.pathname')) {
      return location.state.from.pathname;
    } else if (!ignoreSsrProps && _.has(ssrProps, 'fromUrlRelative')) {
      return ssrProps.fromUrlRelative;
    }
    return globalContext.ssrRoutes.react_app_home_page;
  }, [globalContext.ssrRoutes.react_app_home_page, location, ssrProps])

  useEffect(() => {
    if (!ignoreSsrProps) {
      ignoreSsrProps = true;
    }
  }, []);

  return (
    _.isEmpty(globalContext.stateUserInfo)
      ?
      <>
        <h1>Sign In</h1>

        <LoginForm/>
      </>
      :
      <Navigate to={to} replace/>
  );
};

export {
  LoginPage
};
