import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {CustomerIndexPage} from './CustomerIndexPage';

describe('CustomerIndexPage', () => {
  test('basic rendering', () => {
    render(<CustomerIndexPage/>);
    expect(screen.getByRole('heading', {level: 1})).toBeVisible();
  });
});
