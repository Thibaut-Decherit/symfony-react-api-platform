import {FormGroup} from '@/components/FormGroup';
import {useNavigateBackward} from '@/components/hooks/useNavigateBackward';
import {LockableButton} from '@/components/LockableButton';
import {GlobalContext} from '@/GlobalContextProvider';
import {LodashHelper} from '@/helpers/LodashHelper';
import type {CustomerPageCustomer} from '@/pages/CustomerPage/CustomerPage.types';
import {add, editOneById} from '@/services/APIService/CustomerAPIService';
import {submit} from '@/services/FormSubmissionService';
import type {
  FormErrors,
  GlobalFormAlert,
  ReactBootstrapFormControlChangeEvent,
  ReactBootstrapFormSubmitEventHandler
} from '@/types';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {Dispatch, SetStateAction, useCallback, useContext, useMemo, useState} from 'react';
import {Alert, Form} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

/**
 * @param {CustomerPageCustomer} initialStateCustomer
 * @param {CustomerPageCustomer} parentStateCustomer
 *
 * @param {Dispatch<SetStateAction<CustomerPageCustomer>>} setParentStateCustomer
 * Differentiated from stateCustomer so it won't get updated by handleChange().
 *
 * @param {string} id
 * @return {JSX.Element}
 */
const CustomerNewEditForm = (
  {
    initialStateCustomer,
    parentStateCustomer,
    setParentStateCustomer,
    id
  }: {
    initialStateCustomer: CustomerPageCustomer;
    parentStateCustomer: CustomerPageCustomer;
    setParentStateCustomer: Dispatch<SetStateAction<CustomerPageCustomer>>;
    id: string;
  }
) => {
  const globalContext = useContext(GlobalContext);
  const navigateBackward = useNavigateBackward(globalContext.ssrRoutes.react_app_customer_index_page);

  const [stateCustomer, setStateCustomer] = useState({...parentStateCustomer});
  const initialStateErrors = useMemo(() => {
    return {
      company: [],
      email: [],
      firstName: [],
      lastName: []
    };
  }, []);
  const [stateErrors, setStateErrors] = useState<FormErrors>({...initialStateErrors});
  const initialStateGlobalFormAlert = useMemo(() => {
    return {};
  }, []);
  const [stateGlobalFormAlert, setStateGlobalFormAlert] = useState<GlobalFormAlert>({...initialStateGlobalFormAlert});
  const [stateIsSubmitting, setStateIsSubmitting] = useState(false);

  const handleChange = useCallback(({currentTarget}: ReactBootstrapFormControlChangeEvent) => {
    const {name, value} = currentTarget;
    setStateCustomer({...stateCustomer, [name]: value})
  }, [stateCustomer]);

  const handleNavigateBackward = useCallback(() => {
    navigateBackward();
  }, [navigateBackward]);

  const handleSubmit = useCallback(async (event: ReactBootstrapFormSubmitEventHandler) => {
    event.preventDefault();

    setStateIsSubmitting(true);
    setStateGlobalFormAlert({...initialStateGlobalFormAlert});

    await submit(
      id === 'new' ? add : editOneById,
      id === 'new' ? [stateCustomer] : [id, stateCustomer],
      () => {
        setStateGlobalFormAlert({
          message: id === 'new'
            ? 'Customer created successfully'
            : `Customer ${stateCustomer.firstName} ${stateCustomer.lastName} edited successfully`,
          variant: 'success'
        });

        // Clears all form errors.
        setStateErrors({...initialStateErrors});

        // If id is 'new' it's a new form, else it's an edit form.
        if (id === 'new') {
          // Clears all fields so another Customer can be created right away.
          setStateCustomer({...initialStateCustomer});
        } else {
          // Updates stateCustomer in parent to update the page title.
          setParentStateCustomer({...stateCustomer});
        }
      },
      stateErrors,
      setStateErrors,
      (error: any) => {
        setStateGlobalFormAlert({
          message: error.message === 'Network Error' ? 'Network error' : 'Something went wrong',
          variant: 'danger'
        });
      }
    );

    setStateIsSubmitting(false);
  }, [
    id,
    initialStateCustomer,
    initialStateErrors,
    initialStateGlobalFormAlert,
    setParentStateCustomer,
    stateCustomer,
    stateErrors
  ]);

  const formPrefix = `customer-${id === 'new' ? 'new' : 'edit'}`;
  return (
    <>
      {LodashHelper.hasAll(stateGlobalFormAlert, ['message', 'variant']) && (
        <Alert variant={stateGlobalFormAlert.variant} className="text-center">
          {stateGlobalFormAlert.message}
        </Alert>
      )}
      <Form aria-label={'customer form'} onSubmit={handleSubmit}>
        <FormGroup
          prefix={formPrefix} name="lastName" label="Last name" value={stateCustomer.lastName}
          onChange={handleChange} errorMessages={stateErrors.lastName}
        />
        <FormGroup
          prefix={formPrefix} name="firstName" label="First name" value={stateCustomer.firstName}
          onChange={handleChange} errorMessages={stateErrors.firstName}
        />
        <FormGroup
          prefix={formPrefix} name="email" label="Email address" type="email" value={stateCustomer.email}
          onChange={handleChange} errorMessages={stateErrors.email}
        />
        <FormGroup
          prefix={formPrefix} name="company" label="Company" value={_.defaultTo(stateCustomer.company, '')}
          onChange={handleChange} errorMessages={stateErrors.company} required={false}
        />
        <div className="d-flex justify-content-between">
          <Button onClick={handleNavigateBackward} variant="secondary">
            <span aria-hidden={true} className="fa fa-arrow-left mr-2"/>
            Back
          </Button>
          <LockableButton disabled={stateIsSubmitting} type="submit" variant="success">
            <span aria-hidden={true} className="fa fa-save mr-2" data-lockable-button-icon=""/>
            Submit
          </LockableButton>
        </div>
      </Form>
    </>
  );
};

CustomerNewEditForm.propTypes = {
  initialStateCustomer: PropTypes.shape({
    company: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired
  }).isRequired,
  parentStateCustomer: PropTypes.shape({
    company: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired
  }).isRequired,
  setParentStateCustomer: PropTypes.func.isRequired,
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired
};

export {
  CustomerNewEditForm
};
