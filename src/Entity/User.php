<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups as SerializerGroups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="form_errors.user.unique_email")
 * @ApiResource(
 *     normalizationContext={
 *          "groups"={"user_read"}
 *     }
 * )
 */
class User implements UserInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @SerializerGroups({"customer_read", "invoice_read", "invoice_read_as_subresource", "user_read"})
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=180, unique=true)
     * @SerializerGroups({"customer_read", "invoice_read", "invoice_read_as_subresource", "user_read"})
     * @Assert\Email(message = "form_errors.user.valid_email")
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "form_errors.global.min_length",
     *      maxMessage = "form_errors.global.max_length",
     * )
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $email;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * The hashed password
     *
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     * @SerializerGroups({"customer_read", "invoice_read", "invoice_read_as_subresource", "user_read"})
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "form_errors.global.min_length",
     *      maxMessage = "form_errors.global.max_length",
     * )
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     * @SerializerGroups({"customer_read", "invoice_read", "invoice_read_as_subresource", "user_read"})
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "form_errors.global.min_length",
     *      maxMessage = "form_errors.global.max_length",
     * )
     * @Assert\NotBlank(message="form_errors.global.not_blank")
     */
    private $lastName;

    /**
     * @var Collection|Customer[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Customer", mappedBy="user", orphanRemoval=true)
     */
    private $customers;

    /**
     * @var Collection|UserJWT[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\UserJWT", mappedBy="user", orphanRemoval=true)
     */
    private $JWTs;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->customers = new ArrayCollection();
        $this->roles = ['ROLE_USER'];
        $this->JWTs = new ArrayCollection();
    }

    public function buildFrontEndData(): array
    {
        return [
            'email' => $this->getEmail(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'roles' => $this->getRoles()
        ];
    }

    public function buildFrontEndDataJson(): string
    {
        return json_encode($this->buildFrontEndData());
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection|Customer[]
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function addCustomer(Customer $customer): self
    {
        if (!$this->customers->contains($customer)) {
            $this->customers[] = $customer;
            $customer->setUser($this);
        }

        return $this;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function removeCustomer(Customer $customer): self
    {
        if ($this->customers->contains($customer)) {
            $this->customers->removeElement($customer);
            // set the owning side to null (unless already changed)
            if ($customer->getUser() === $this) {
                $customer->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserJWT[]
     */
    public function getJWTs(): Collection
    {
        return $this->JWTs;
    }

    public function addJWT(UserJWT $jWT): self
    {
        if (!$this->JWTs->contains($jWT)) {
            $this->JWTs[] = $jWT;
            $jWT->setUser($this);
        }

        return $this;
    }

    public function removeJWT(UserJWT $jWT): self
    {
        if ($this->JWTs->contains($jWT)) {
            $this->JWTs->removeElement($jWT);
            // set the owning side to null (unless already changed)
            if ($jWT->getUser() === $this) {
                $jWT->setUser(null);
            }
        }

        return $this;
    }
}
