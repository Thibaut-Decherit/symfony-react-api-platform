<?php

namespace App\Controller\ReactApp;

use App\Controller\AbstractDefaultController;
use App\Entity\Customer;
use App\Entity\Invoice;
use App\Service\ReactAppRoutesGeneratorService;
use InvalidArgumentException;
use stdClass;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractReactAppController extends AbstractDefaultController
{
    private ReactAppRoutesGeneratorService $routesGeneratorService;

    public function __construct(ReactAppRoutesGeneratorService $routesGeneratorService)
    {
        parent::__construct();

        $this->routesGeneratorService = $routesGeneratorService;
    }

    // Builds props that are always passed to React no matter the route.
    private function buildBaseProps(Request $request): array
    {
        $baseProps = [];

        $baseProps['ssrRoute'] = $request->getPathInfo();

        $baseProps['ssrRoutes'] = $this->routesGeneratorService->generate();

        $baseProps['userInfo'] = $this->getUser()?->buildFrontEndData();

        return $baseProps;
    }

    protected function buildProps(Request $request, array $routeSpecificProps = []): array
    {
        $props = $this->buildBaseProps($request);

        // React app is expecting an object, even if it's empty, but not an empty array.
        if (empty($routeSpecificProps)) {
            $routeSpecificProps = new stdClass();
        }

        $props['routeSsrProps'] = $routeSpecificProps;

        return $props;
    }

    /**
     * Ensures search parameters are valid (e.g. not a letter instead of an integer).
     * If a parameter is invalid it is replaced with a valid default value.
     *
     * @param string $entityClass
     * @param array $searchParams
     * @return array
     * [
     *      'has_errors' => boolean, you might want to refresh the page if it's true,
     *      'search_params' => array, the cleaned up parameters
     * ]
     */
    protected function cleanupSearchParams(string $entityClass, array $searchParams): array
    {
        $supportedEntityClasses = [
            Customer::class,
            Invoice::class
        ];
        if (!in_array($entityClass, $supportedEntityClasses)) {
            throw new InvalidArgumentException(
                "Class $entityClass is not supported."
            );
        }

        $hasErrors = false;

        if (
            array_key_exists('itemsPerPage', $searchParams)
            && (int)$searchParams['itemsPerPage'] < 1
        ) {
            $hasErrors = true;
            $searchParams['itemsPerPage'] = $entityClass::DEFAULT_SEARCH_PARAMS['itemsPerPage'];
        }

        if (
            array_key_exists('page', $searchParams)
            && (int)$searchParams['page'] < 1
        ) {
            $hasErrors = true;
            $searchParams['page'] = $entityClass::DEFAULT_SEARCH_PARAMS['page'];
        }

        return [
            'has_errors' => $hasErrors,
            'search_params' => array_merge($entityClass::DEFAULT_SEARCH_PARAMS, $searchParams)
        ];
    }

    protected function buildDocumentTitle(string $title = '', string $baseTitle = 'SF React API Platform'): string
    {
        if ($title !== '') {
            return $title . ' | ' . $baseTitle;
        }

        return $baseTitle;
    }
}
