<?php

namespace App\Doctrine\CurrentUserExtension\CurrentUserExtensionEntity;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

interface CurrentUserExtensionEntityInterface
{
    public function addWhereCurrentUser(
        QueryBuilder $queryBuilder,
        string $rootAlias,
        string $userParameterName,
        QueryNameGeneratorInterface $queryNameGenerator
    ): void;
}
