export type InvoiceListInvoice = {
  amount: string;
  chrono: number;
  customer: {
    company: string | null;
    email: string;
    firstName: string;
    id: number;
    lastName: string;
  };
  id: number;
  sentAt: string;
  status: 'CANCELLED'|'PAID'|'SENT';
};
