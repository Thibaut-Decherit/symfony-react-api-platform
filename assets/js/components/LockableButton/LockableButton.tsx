import {TwigDataService} from '@/services/TwigDataService';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {Children, useEffect, useState} from 'react';
import {Button, Spinner} from 'react-bootstrap';
import type {ReactNodeType} from './LockableButton.types';

/*
If the button contains a Font Awesome icon, ensure the icon element has the `data-lockable-button-icon=""` attribute, or
it won't be replaced by a spinner when disabled prop is true.
 */
const LockableButton = (
  props: {
    children: ReactNodeType|ReactNodeType[];
    disabled: boolean;
    [key: string]: unknown;
  }
) => {
  const {children, disabled} = props;
  
  const [stateFilteredChildren, setStateFilteredChildren] = useState<ReactNodeType[]>([]);

  const renderSpinner = (className = 'mr-2', key = 0) => {
    return (
      <Spinner animation="border" className={className.trim()} key={key} role="status" size="sm">
        <span className="sr-only">{TwigDataService.get('translations.global.loading')}</span>
      </Spinner>
    );
  };

  // Replaces with a spinner any children having the data-is-promise-lockable-button-icon="true" attribute.
  useEffect(() => {
    const replaceIcon = () => {
      let unfilteredChildren: ReactNodeType[] = [];

      // Used instead of children = [children]; in if without else, as that code confuses Typescript
      if (!Array.isArray(children)) {
        unfilteredChildren.push(children);
      } else {
        unfilteredChildren = children;
      }

      const filteredChildren = _.cloneDeep<ReactNodeType[]>(unfilteredChildren);
      let addDefaultSpinner = true;
      Children.forEach(
        children,
        (
          child: ReactNodeType,
          index
        ) => {
          // Skips child if it should not be replaced.
          if (!_.has(child, 'props.data-lockable-button-icon')) {
            return;
          }

          addDefaultSpinner = false;

          let className = child?.props?.className ?? '';

          // Removes Font Awesome classes.
          className = className.replace(/(?:^| )(?:fa|fab|fad|fal|far|fas) fa-(?:\w|-)+/, '');

          filteredChildren[index] = renderSpinner(className, index);
        });

      // No eligible element has been replaced by a spinner, so we need to 'manually' prepend a spinner to the children.
      if (addDefaultSpinner) {
        filteredChildren.unshift(renderSpinner());
      }

      return filteredChildren;
    };

    setStateFilteredChildren(replaceIcon());
  }, [children]);

  return (
    <Button {...props}>
      {disabled
        ? (
          stateFilteredChildren
        )
        : (
          children
        )
      }
    </Button>
  );
};

LockableButton.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.string
  ]).isRequired,
  disabled: PropTypes.bool.isRequired
};

export {
  LockableButton
};
