export type CustomerPageCustomer = {
  company: string | null;
  email: string;
  firstName: string;
  lastName: string;
  [key: string]: unknown;
};
