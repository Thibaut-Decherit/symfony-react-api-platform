import {getJWT} from '@/services/APIService/AuthenticationAPIService';
import type {GetJWTOnSuccessCallback} from '@/services/APIService/AuthenticationAPIService.types';
import {TwigDataService} from '@/services/TwigDataService';
import type {Credentials} from '@/types';
import axios from 'axios';

const login = (
  credentials: Credentials,
  onSuccess?: GetJWTOnSuccessCallback
) => {
  return new Promise<void>((resolve, reject) => {
    getJWT(credentials, onSuccess)
      .then(() => {
        resolve();
      })
      .catch((error: Error) => {
        reject(error);
      })
  });
};

const logout = (onSuccess?: GetJWTOnSuccessCallback) => {
  return new Promise<void>((resolve, reject) => {
    axios
      .post(TwigDataService.get<string>('paths.logout'))
      .then(response => {
        if (typeof onSuccess === 'function') {
          onSuccess(response);
        }

        resolve();
      })
      .catch(error => {
        reject(error);
      });
  });
};

export {
  login,
  logout
};
