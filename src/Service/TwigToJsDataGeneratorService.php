<?php

namespace App\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TwigToJsDataGeneratorService
 *
 * Provides a method to generate a JSON object to pass server side data to client side JS via a #twig-to-js-data DOM
 * element.
 *
 * @package App\Service
 */
class TwigToJsDataGeneratorService
{
    private UrlGeneratorInterface $urlGenerator;
    private TranslatorInterface $translator;

    public function __construct(UrlGeneratorInterface $urlGenerator, TranslatorInterface $translator)
    {
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
    }

    public function generate(): string
    {
        return json_encode([
            'paths' => $this->buildPaths(),
            'translations' => $this->buildTranslations()
        ]);
    }

    private function buildPaths(): array
    {
        return [
            'logout' => $this->urlGenerator->generate('logout')
        ];
    }

    private function buildTranslations(): array
    {
        return [
            'global' => [
                'loading' => $this->translator->trans('global.loading')
            ]
        ];
    }
}
