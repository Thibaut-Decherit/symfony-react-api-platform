<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\UserJWT;
use App\Service\UniqueRandomDataGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

/**
 * Class JWTCreatedListener
 * @package App\EventListener
 */
class JWTCreatedListener
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var UniqueRandomDataGeneratorService
     */
    private $uniqueRandomDataGeneratorService;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * JWTCreatedListener constructor
     *
     * @param RequestStack $requestStack
     * @param Security $security
     * @param UniqueRandomDataGeneratorService $uniqueRandomDataGeneratorService
     * @param EntityManagerInterface $em
     */
    public function __construct(
        RequestStack $requestStack,
        Security $security,
        UniqueRandomDataGeneratorService $uniqueRandomDataGeneratorService,
        EntityManagerInterface $em
    )
    {
        $this->requestStack = $requestStack;
        $this->security = $security;
        $this->uniqueRandomDataGeneratorService = $uniqueRandomDataGeneratorService;
        $this->em = $em;
    }

    /**
     * @param JWTCreatedEvent $event
     * @throws Exception
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        /**
         * @var User $user
         */
        $user = $this->security->getUser();
        $payload = $event->getData();

        $uniqueId = $this->uniqueRandomDataGeneratorService->uniqueRandomString(UserJWT::class, 'uniqueId');

        $this->em->persist(new UserJWT($user, $this->requestStack->getMasterRequest(), $uniqueId));
        $this->em->flush();

        $payload['uniqueId'] = $uniqueId;

        $event->setData($payload);
    }
}
