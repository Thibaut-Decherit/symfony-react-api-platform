import {useCallback, useState} from 'react';
import type {ToggleMaintainFocusOffCallback, ToggleMaintainFocusOnCallback} from './useMaintainFocus.types';

const useMaintainFocus = (): [boolean, ToggleMaintainFocusOnCallback, ToggleMaintainFocusOffCallback] => {
  const [stateMaintainFocus, setStateMaintainFocus] = useState(false);
  const toggleMaintainFocusOn = useCallback(() => {
    setStateMaintainFocus(true);
  }, [setStateMaintainFocus]);
  const toggleMaintainFocusOff = useCallback(() => {
    setStateMaintainFocus(false);
  }, [setStateMaintainFocus]);

  return [stateMaintainFocus, toggleMaintainFocusOn, toggleMaintainFocusOff];
}

export {
  useMaintainFocus
};
