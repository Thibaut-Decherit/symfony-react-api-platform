<?php

namespace App\Doctrine\CurrentUserExtension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Doctrine\CurrentUserExtension\CurrentUserExtensionEntity\CurrentUserExtensionCustomer;
use App\Doctrine\CurrentUserExtension\CurrentUserExtensionEntity\CurrentUserExtensionEntityInterface;
use App\Doctrine\CurrentUserExtension\CurrentUserExtensionEntity\CurrentUserExtensionInvoice;
use App\Doctrine\CurrentUserExtension\CurrentUserExtensionEntity\CurrentUserExtensionUser;
use App\Entity\Customer;
use App\Entity\Invoice;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

/**
 * Class CurrentUserExtension
 * @package App\Doctrine\CurrentUserExtension
 *
 * Adds user = currentUser parameter to Customer and Invoice queries so users cannot make any read or write query to a
 * Customer or Invoice entity they don't own.
 * It will effectively prevent IDORs on these entities.
 *
 * API Platform will return 404 response if the targeted resource is not owned by the current user (e.g. PUT at
 * api/invoices/14627 but Invoice 14627 is not owned by current user)
 * API Platform will return 400 response if the reference of a key in the query's body is not owned by the current user
 * (e.g. Invoice POST with key "customer": "api/customers/2864" but Customer 2864 is not owned by current user)
 */
class CurrentUserExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private const SUB_EXTENSIONS = [
        Customer::class => CurrentUserExtensionCustomer::class,
        Invoice::class => CurrentUserExtensionInvoice::class,
        User::class => CurrentUserExtensionUser::class
    ];

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    private function supports(string $resourceClass): bool
    {
        return
            !$this->security->isGranted('ROLE_ADMIN')
            && array_key_exists($resourceClass, self::SUB_EXTENSIONS);
    }

    private function addWhereCurrentUser(
        string $resourceClass,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator
    ): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];

        // Generates a unique parameter name to avoid collisions with other extensions or filters.
        $userParameterName = $queryNameGenerator->generateParameterName('user');

        $this->getSubExtension($resourceClass)->addWhereCurrentUser(
            $queryBuilder,
            $rootAlias,
            $userParameterName,
            $queryNameGenerator
        );

        $queryBuilder->setParameter($userParameterName, $this->security->getUser());
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void
    {
        if (!$this->supports($resourceClass)) {
            return;
        }

        $this->addWhereCurrentUser($resourceClass, $queryBuilder, $queryNameGenerator);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void
    {
        if (!$this->supports($resourceClass)) {
            return;
        }

        $this->addWhereCurrentUser($resourceClass, $queryBuilder, $queryNameGenerator);
    }

    private function getSubExtension(string $resourceClass): CurrentUserExtensionEntityInterface
    {
        return new (self::SUB_EXTENSIONS[$resourceClass])();
    }
}
