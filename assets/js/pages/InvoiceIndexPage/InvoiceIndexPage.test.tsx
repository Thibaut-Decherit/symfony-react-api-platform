import {render, screen} from '@/testing/reactTestingLibraryOverrides';
import React from 'react';
import {InvoiceIndexPage} from './InvoiceIndexPage';

describe('InvoiceIndexPage', () => {
  test('basic rendering', () => {
    render(<InvoiceIndexPage/>);
    expect(screen.getByRole('heading', {level: 1})).toBeVisible();
  });
});
