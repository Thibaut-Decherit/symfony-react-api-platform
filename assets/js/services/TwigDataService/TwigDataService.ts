import _ from 'lodash';

class TwigDataService {
  static get<T>(key: string): T {
    return _.get(
      JSON.parse(document?.getElementById('twig-to-js-data')?.getAttribute('data-twig-to-js') || '{}'),
      key
    );
  }
}

export {
  TwigDataService
};
