<?php

namespace App\EventListener;

use App\Entity\UserJWT;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Class JWTAuthenticatedListener
 * @package App\EventListener
 */
class JWTAuthenticatedListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Throws 401 exception if JWT does not have a matching UserJWT in database, or it is expired.
     * If the matching UserJWT is expired, it is removed from database.
     *
     * @param JWTAuthenticatedEvent $event
     * @throws Exception
     */
    public function onJWTAuthenticated(JWTAuthenticatedEvent $event): void
    {
        $existingUserJWT = $this->em->getRepository(UserJWT::class)->findOneBy([
            'uniqueId' => $event->getPayload()['uniqueId'] ?? null
        ]);

        if (is_null($existingUserJWT) || $existingUserJWT->isExpired()) {
            if (!is_null($existingUserJWT)) {
                $this->em->remove($existingUserJWT);
                $this->em->flush();
            }

            throw new UnauthorizedHttpException('Bearer', 'JWT is expired.');
        }
    }
}
